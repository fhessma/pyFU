#!/bin/sh
# pyfu_bin_links.sh	- creates executables from the core pyFU scripts
#
# EDIT THESE PATHS FOR YOUR OS AND PYTHON VERSION
#PYFUBIN=~/bin
PYFUBIN=/opt/local/bin
PYFUDIR=/opt/local/Library/Frameworks/Python.framework/Versions/3.5/lib/python3.5/site-packages/pyFU
#
if ["$1" == "remove"]; then
	rm ${PYFUBIN}/pyfucalib
	rm ${PYFUBIN}/pyfudisplay
	rm ${PYFUBIN}/pyfuextract
	rm ${PYFUBIN}/pyfufake
	rm ${PYFUBIN}/pyfuimage
	rm ${PYFUBIN}/pyfurebin
	rm ${PYFUBIN}/pyfusolar
	rm ${PYFUBIN}/pyfuwavcal
else
	ln -s ${PYFUDIR}/calib.py   ${PYFUBIN}/pyfucalib
	ln -s ${PYFUDIR}/display.py ${PYFUBIN}/pyfudisplay
	ln -s ${PYFUDIR}/extract.py ${PYFUBIN}/pyfuextract
	ln -s ${PYFUDIR}/fake.py    ${PYFUBIN}/pyfufake
	ln -s ${PYFUDIR}/image.py   ${PYFUBIN}/pyfuimage
	ln -s ${PYFUDIR}/rebin.py   ${PYFUBIN}/pyfurebin
	ln -s ${PYFUDIR}/solar.py   ${PYFUBIN}/pyfusolar
	ln -s ${PYFUDIR}/wavcal.py  ${PYFUBIN}/pyfuwavcal
	chmod +x ${PYFUBIN}/pyfucalib
	chmod +x ${PYFUBIN}/pyfudisplay
	chmod +x ${PYFUBIN}/pyfuextract
	chmod +x ${PYFUBIN}/pyfufake
	chmod +x ${PYFUBIN}/pyfuimage
	chmod +x ${PYFUBIN}/pyfurebin
	chmod +x ${PYFUBIN}/pyfusolar
	chmod +x ${PYFUBIN}/pyfuwavcal
fi

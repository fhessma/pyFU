#!/usr/bin/env python3

# test/calib.py 

import enum
import matplotlib.pyplot as plt
import logging
import numpy as np
import os, sys

from astropy.io    import fits
from pyFU.utils    import get_list_of_paths_and_filenames
from pyFU.defaults import defaultKeywords

logging.basicConfig (level=logging.INFO,format='%(asctime)s * %(levelname)s * %(message)s')

class SimplePipeline (object) :
	"""
	Simple object for reducing raw data.
	"""
	def __init__ (self, keywords=defaultKeywords) :
		self.keywords = keywords
		self.master_bias = None
		self.unit_dark   = None
		self.master_flat = None

	def calculate_bias (self, imagelist, show=False, outfile=None) :
		"""
		Returns the median bias from a list of bias images.
		"""
		if imagelist is None or len(imagelist) == 0 :
			logging.error ('no list of bias images')
			return None
		n = len(imagelist)
		logging.info ('calculating median bias from {0} input files...'.format(n))

		# GET SHAPE OF DATA
		name = imagelist[0]
		hdus = fits.open (name)
		hdu = hdus[-1]
		ny,nx = hdu.data.shape
		header = hdu.header.copy()
		raw = hdu.data
		med,std = np.nanmedian(raw),np.nanstd(raw)
		logging.info ('... {0} : med={1:.2f},std={2:.2f}'.format(name,med,std))

		# PUT ALL DATA INTO AN ARRAY
		data = np.zeros ((n,ny,nx))
		data[0,:,:] = hdu.data
		hdus.close()
		for i in range(1,n) :
			l = imagelist[i]
			hdus = fits.open (l)
			raw = hdus[-1].data
			data[i,:,:] = raw
			med,std = np.nanmedian(raw),np.nanstd(raw)
			logging.info ('... {0} : med={1:.2f},std={2:.2f}'.format(name,med,std))
			hdus.close()

		# GET THE MEDIAN
		self.master_bias = np.nanmedian (data,axis=0)
		med,std = np.nanmedian(self.master_bias),np.nanstd(self.master_bias)
		logging.info ('master bias: median={0:.2f}, std={1:.2f}'.format(med,std))

		# SHOW?
		if show :
			im = plt.imshow (self.master_bias, interpolation='none', origin='lower', vmin=med-3*std, vmax=med+3*std)
			plt.colorbar(im)
			plt.title ('master bias')
			plt.show ()

		# SAVE?
		if outfile is not None :
			logging.info ('writing master bias to {0} ...'.format(outfile))
			hdu = fits.PrimaryHDU(self.master_bias)
			hdr = hdu.header
			hdr['comment'] = 'median of {0} biases'.format(len(infiles))
			hdr.extend (header,update=False)
			hdu.writeto (outfile)

		return self.master_bias

	def calculate_unit_dark (self, imagelist, method='median', bias=None, subtract_bias=True, show=False, outfile=None) :
		"""
		Returns the unit dark frame from a list of dark images.
		"""
		if imagelist is None or len(imagelist) == 0 :
			logging.error ('no list of dark images')
			return None
		n = len(imagelist)
		logging.info ('calculating unit dark from {0} input files...'.format(n))

		# GET SHAPE OF DATA
		name = imagelist[0]
		hdus = fits.open (name)
		hdu = hdus[-1]
		ny,nx = hdu.data.shape
		data = np.zeros ((n,ny,nx))
		header = hdu.header.copy()
		raw = hdu.data
		med,std = np.nanmedian(raw),np.nanstd(raw)
		logging.info ('... {0} : med={1:.2f},std={2:.2f}'.format(name,med,std))
		hdus.close()

		# PUT ALL DATA INTO AN ARRAY
		for i in range(n) :
			name = imagelist[i]
			hdus = fits.open (name)
			hdu = hdus[-1]
			raw = hdu.data
			med,std = np.nanmedian(raw),np.nanstd(raw)
			logging.info ('... {0} : med={1:.2f},std={2:.2f}'.format(name,med,std))

			# GET EXPOSURE TIME
			hdr = hdu.header
			t = 1.
			if 'exptime' in self.keywords and self.keywords['exptime'][0] in hdr :
				keyw = self.keywords['exptime'][0]
				t = hdr[keyw]
			else :
				logging.warning ('unable to access exposure time in {0}'.format(name))

			# SUBTRACT BIAS, DIVIDE BY EXPTIME ...
			if subtract_bias :
				if bias is None :
					b = self.master_bias
				else :
					b = bias
				if b is None :
					logging.error ('no bias available to subtract from dark!')
				elif isinstance(b,float) or isinstance(b,int) or b.shape == raw.shape :
					data[i,:,:] = (raw-b)/t
				else :
					logging.error ('data and bias images have different shapes!')
					return None
			# ...OR IGNORE BIAS
			else :
				data[i,:,:] = raw/t
			hdus.close()

		# GET THE UNIT DARK
		if method == 'median' :
			self.unit_dark = np.median (data,axis=0)
		else :
			self.unit_dark = np.mean (data,axis=0)
		med,std = np.nanmedian(self.unit_dark),np.nanstd(self.unit_dark)
		logging.info ('unit dark: median={0:.2f}, std={1:.2f}'.format(med,std))

		# SHOW?
		if show :
			im = plt.imshow (self.unit_dark, interpolation='none', origin='lower', vmin=med-3*std, vmax=med+3*std)
			plt.colorbar(im)
			plt.title ('unit dark')
			plt.show ()

		# SAVE?
		if outfile is not None :
			logging.info ('writing unit dark to {0} ...'.format(outfile))
			hdu = fits.PrimaryHDU(self.unit_dark)
			hdr = hdu.header
			hdr['EXPTIME'] = (1.0,'unit exposure time of 1 sec')
			hdr['comment'] = 'median of {0} unit darks'.format(len(infiles))
			hdr.extend (header,update=False)
			hdu.writeto (outfile)

		return self.unit_dark

	def calculate_flat (self, imagelist, bias=None, unitdark=None, subtract_bias=True, subtract_dark=True, show=False) :
		"""
		Returns the median scaled flatfield frame from a list of flatfield images.
		"""
		if imagelist is None or len(imagelist) == 0 :
			logging.error ('no list of flat images')
			return None
		n = len(imagelist)
		hdus = fits.open (imagelist[0])

		# GET SHAPE OF DATA
		hdu = hdus[-1]
		ny,nx = hdu.data.shape
		raw = hdu.data
		med,std = np.nanmedian(raw),np.nanstd(raw)
		logging.info ('... {0} : med={1:.2f},std={2:.2f}'.format(name,med,std))
		hdus.close()

		# PUT ALL DATA INTO AN ARRAY
		data = np.zeros ((n,ny,nx))
		for i in range(n) :
			name = imagelist[i]
			hdus = fits.open (name)
			hdu = hdus[-1]
			raw = hdu.data
			med,std = np.nanmedian(raw),np.nanstd(raw)
			logging.info ('... {0} : med={1:.2f},std={2:.2f}'.format(name,med,std))

			# GET EXPOSURE TIME
			hdr = hdu.header
			t = 1.
			if 'exptime' in self.keywords and self.keywords['exptime'][0] in hdr :
				keyw = self.keywords['exptime'][0]
				t = hdr[keyw]
			elif subtract_dark :
				logging.warning ('unable to access exposure time in {0}'.format(name))

			# GET BIAS
			b = 0.
			if subtract_bias :
				if bias is None :
					b = self.master_bias
				else :
					b = bias
				if b is None :
					logging.error ('no bias available to subtract from flat!')
				elif not (isinstance(b,float) or isinstance(b,int) or b.shape == raw.shape) :
					logging.error ('data and bias images have different shapes!')
					return None

			# GET UNIT DARK
			d = 0.
			if subtract_dark :
				if unitdark is None :
					d = self.unit_dark
				else :
					d = unitdark
				if d is None :
					logging.error ('no unit dark available to subtract from flat!')
				elif not (isinstance(d,float) or isinstance(d,int) or b.shape == raw.shape) :
					logging.error ('data and dark images have different shapes!')
					return None

			# CALIBRATE
			cal = raw-b-d*t

			# NORMALIZE
			data[i,:,:] = cal/np.median(cal)
			hdus.close()

		# GET THE UNIT MEDIAN FLATFIELD
		self.master_flat = np.median (data,axis=0)
		med,std = np.nanmedian(self.master_flat),np.nanstd(self.master_flat)
		logging.info ('master flat: median={0:.2f}, std={1:.2f}'.format(med,std))

		# SHOW?
		if show :
			im = plt.imshow (self.master_flat, interpolation='none', origin='lower', vmin=med-3*std, vmax=med+3*std)
			plt.colorbar(im)
			plt.title ('master flat')
			plt.show ()

		# SAVE?
		if outfile is not None :
			logging.info ('writing master flat to {0} ...'.format(outfile))
			fits.PrimaryHDU(self.master_flat)
			hdr = hdu.header
			hdr['comment'] = 'median of {0} normalized flats'.format(len(infiles))
			hdr.extend (header,update=False)
			hdu.writeto (outfile)

		return self.unit_dark

	def calibrate (self, hdu, bias=None, unitdark=None, flat=None, subtract_bias=False, subtract_dark=False, divide_flat=False, show=False) :
		data = hdu.data
		hdr = hdu.header
		ny,nx = data.shape

		# GET BIAS
		b = 0.
		if subtract_bias :
			if bias is None :
				b = self.master_bias
			else :
				b = bias
			if b is None :
				logging.error ('no bias to subtract')
				return False
			hdr['comment'] = 'subtracted master bias'

		# GET DARK
		d = 0.
		t = 1.
		if subtract_dark :
			if 'exptime' in self.keywords and self.keywords['exptime'][0] in hdr :
				keyw = self.keywords['exptime'][0]
				t = hdr[keyw]
			else :
				logging.warning ('unable to access exposure time in {0}'.format(name))
			if unitdark is None :
				d = self.unit_dark
			else :
				d = unitdark
			if d is None :
				logging.error ('no unit dark to subtract')
				return False
			hdr['comment'] = 'subtracted dark from unit dark'

		# GET FLAT
		f = 1.
		if divide_flat :
			if flat is None :
				f = self.master_flat
			else :
				f = flat
			if f is None :
				logging.error ('no flat to divide')
				return False
			hdr['comment'] = 'divided by flatfield'

		# CALIBRATE
		result = (data-b-d*t)/f
		fmt = 'raw: med,std={0:.2f},{1:.2f}, result: med,std={2:.2f},{3:.2f}'
		logging.info (fmt.format(np.nanmedian(data),np.nanstd(data),np.nanmedian(result),np.nanstd(result)))
		hdu.data = result

		# SHOW?
		if show :
			show_hdu (hdu)
			if 'FILENAME' in hdu.header :
				plt.title ('calibrated'+hdu.header['FILENAME'])
			plt.show ()

		return True 

	def maths (self, thing1=None, oper=None, thing2=None) :
		"""
		Function for doing simple maths of the form "thing1 + thing2" with images.
		"""
		# GET THING1
		img1 = False
		if '.fit' in thing1 :
			hdus1 = fits.open (thing1)
			data1 = np.array(hdus1[-1].data,dtype=float)
			img1 = True
		elif thing1 is not None :
			data1 = float(thing1)

		# GET THING2
		img2 = False
		if '.fit' in thing2 :
			hdus2 = fits.open (thing2)
			data2 = np.array(hdus2[-1].data,dtype=float)
			img2 = True
		else :
			data2 = float(thing2)

		# PERFORM OPERATION thing1 oper thing2 OR oper thing2
		if oper == '+' :
			data3 = data1+data2
		elif oper == '-' :
			if thing1 is None :
				data3 = data1-data2
			else :
				data3 = -data2
		elif oper == '*' :
			data3 = data1*data2
		elif oper == '/' :
			data3 = data1/data2
		elif oper == '^' or oper == '**' :
			data3 = data1**data2
		elif thing1 is None and oper == 'sqrt' :
			data3 = np.sqrt(data2)
		elif thing1 is None and oper == 'abs' :
			data3 = np.abs(data2)

		if img1 or img2 :
			if img1 :
				hdu = hdus1[-1]
				hdu.data = data3
				return hdus1[-1]
			else :
				hdu = hdus2[-1]
				hdu[-1].data = data3
			hdu.header['comment'] = 'data is now: {0} {1} {2}'.format(thing1,oper,thing2)
			return hdu
		else :
			return data3

if __name__ == '__main__' :
	import yaml
	from pyFU.utils import parse_arguments
	from pyFU.display import show_hdu

	logging.info ('*************************** simple raw image pipeline ******************************')

	# ---- GET DEFAULTS AND PARSE COMMAND LINE
	arguments = {
		'biassec': {'path':'calib:',
			'default':None, 'dshow':None, 'flg':'-S','type':list,'help':'bias section x1,x2,y1,y2 (numpy coords)'},
		'bias_files': {'path':'calib:bias:infiles',
			'default':None, 'dshow':None, 'flg':'-1','type':str,'help':'pattern for raw bias pathnames'},
		'dark_files': {'path':'calib:dark:infiles',
			'default':None, 'dshow':None, 'flg':'-2','type':str,'help':'pattern for raw dark pathnames'},
		'divide_flat': {'path':'calib:flat:',
			'default':None, 'dshow':None, 'flg':'-F','type':bool,'help':'subtract scaled unit dark from image'},
		'flat_files': {'path':'calib:flat:infiles',
			'default':None, 'dshow':None, 'flg':'-3','type':str,'help':'pattern for raw flat pathnames'},
		'infile':    {'path':'calib:',
			'default':None, 'dshow':None, 'flg':'-i','type':str,'help':'name of FITS image file to process'},
		'masterbias': {'path':'calib:bias:',
			'default':None, 'dshow':None, 'flg':'-b','type':str,'help':'pathname of master bias image'},
		'masterflat': {'path':'calib:flat:',
			'default':None, 'dshow':None, 'flg':'-f','type':str,'help':'pathname of master flatfield image'},
	    'plot':      {'path':None,
			'default':False,'dshow':False,'flg':'-p','type':bool,'help':'plot details'},
		'subtract_bias': {'path':'calib:bias:',
			'default':None, 'dshow':None, 'flg':'-B','type':bool,'help':'subtract master bias from image'},
		'subtract_dark': {'path':'calib:dark:',
			'default':None, 'dshow':None, 'flg':'-D','type':bool,'help':'subtract scaled unit dark from image'},
		'outfile':   {'path':'calib:',
			'default':None, 'dshow':None, 'flg':'-o','type':str,'help':'pathname of output FITS image file'},
		'trimsec': {'path':'calib:',
			'default':None, 'dshow':None, 'flg':'-T','type':list,'help':'trim section x1,x2,y1,y2 (numpy coords)'},
		'unitdark': {'path':'calib:dark:',
			'default':None, 'dshow':None, 'flg':'-d','type':str,'help':'pathname of unit dark image'},
		'yaml':      {'path':None,
			'default':None, 'dshow':None, 'flg':'-y','type':str,'help':'global YAML configuration file for parameters'}
		}
	args,cfg = parse_arguments (arguments)
	info = cfg['calib']

	# ---- GET SIMPLE PIPELINE OBJECT
	pipel = SimplePipeline ()
	sub_bias = False
	sub_dark = False
	div_flat = False

	# ---- GET INPUT DATA
	if 'infile' not in info or info['infile'] is None :
		print ('no input filename!')
		hdus = None
		hdu = None
	else :
		logging.info ('infile: {0}'.format(info['infile']))
		hdus = fits.open (info['infile'],mode='update')
		hdu = hdus[-1]

	# ---- CHECK FOR BIAS
	dbias = info['bias']
	if 'subtract_bias' in dbias and dbias['subtract_bias'] :
		if not 'masterbias' in dbias :
			logging.error ('no master bias image pathname given')
			sys.exit(1)
		# ALREADY EXISTS?
		elif os.path.isfile (dbias['masterbias']) :
			bhdus = fits.open (dbias['masterbias'])
			pipel.master_bias = bhdus[-1].data
		# CAN BE CREATED?
		elif 'infiles' in dbias :
			infiles = dbias['infiles']
			if isinstance (infiles,str) :
				infiles = get_list_of_paths_and_filenames (infiles,mode='path')
			pipel.calculate_bias (infiles, show=args.plot, outfile=dbias['masterbias'])
		sub_bias = True
	else :
		logging.info ('no bias subtraction wanted')

	# ---- CHECK FOR UNIT DARK
	ddark = info['dark']
	if 'subtract_dark' in ddark and ddark['subtract_dark'] :
		if not 'unitdark' in ddark :
			logging.error ('no master unit dark image pathname given')
			sys.exit(1)
		# ALREADY EXISTS?
		elif os.path.isfile (ddark['unitdark']) :
			dhdus = fits.open (ddark['unitdark'])
			pipel.unit_dark = dhdus[-1].data
		# CAN BE CREATED?
		elif 'infiles' in ddark :
			infiles = ddark['infiles']
			if isinstance (infiles,str) :
				infiles = get_list_of_paths_and_filenames (infiles,mode='path')
			pipel.calculate_unit_dark (infiles, show=args.plot, outfile=ddark['unitdark'])
		sub_dark = True
	else :
		logging.info ('no dark subtraction wanted')

	# ---- CHECK FOR MASTER FLAT
	f = None
	dflat = info['flat']
	if 'divide_flat' in dflat and dflat['divide_flat'] :
		if not 'masterflat' in dflat :
			logging.error ('no master flatfield image pathname given')
			sys.exit(1)
		# ALREADY EXISTS?
		elif os.path.isfile (dflat['masterflat']) :
			dhdus = fits.open (dflat['masterflat'])
			pipel.master_flat = dhdus[-1].data
		# CAN BE CREATED?
		elif 'infiles' in dflat :
			infiles = dflat['infiles']
			if isinstance (infiles,str) :
				infiles = get_list_of_paths_and_filenames (infiles,mode='path')
			pipel.calculate_flat (infiles, show=args.plot, outfile=dflat['masterflat'])
		div_flat = True
	else :
		logging.info ('no flatfielding wanted')

	# ---- CALIBRATE
	if hdu is not None and (sub_bias or sub_dark or div_flat) :
		if not pipel.calibrate (hdu, subtract_bias=sub_bias, subtract_dark=sub_dark, divide_flat=div_flat) :
			logging.error ('could not calibration image')
			sys.exit (1)

		"""
		# ---- TRIM
		if 'trimsec' in info and info['trimsec'] is not None :
			x1,x2,y1,y2 = info['trimsec']	# x1,x2,y1,y2 IN numpy COORDINATES
			logging.info ('trimming calibrated image ...')
			hdu.data = hdu.data[x1:x2, y1:y2]
			hdr['comment'] = 'trimmed to [{0}:{1}, {2}:{3}]'.format(x1,x2,y1,y2)
		"""

		# ---- PLOT
		if args.plot and hdu is not None :
			show_hdu (hdu)
			plt.title ('reduced {0}'.format(info['infile']))
			plt.show ()

		# ---- SAVE RESULT
		if 'outfile' in info and info['outfile'] is not None :
			logging.info ('writing calibrated image to {0}'.format(info['outfile']))
			hdu.writeto (info['outfile'],overwrite=True)


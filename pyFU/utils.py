# pyFU/utils.py

import argparse
import bisect
import datetime
import logging
import numpy as np
import os
import parse
import sys
import yaml

from astropy.io	   import fits
from astropy.table import Table, Column
from scipy		   import signal, optimize, integrate
from scipy.ndimage import gaussian_filter1d

logging.basicConfig (level=logging.INFO,format='%(asctime)s * %(levelname)s * %(message)s')


PYFU_EXTENSIONS = {
		'.FIT':'fits',
		'.fit':'fits',
		'.fits':'fits',
		'.xml':'votable',
		'.vot':'votable',
		'.dat':'ascii',
		'.txt':'ascii',
		'.csv':'ascii.csv'
		}

def convolve_rebin_table (tab, dlambda=None, wave_out=None, model='interpolate',
			pixcol='pixel', wavcol='wavelength', flxcol='flux', errcol='err_flux', other=None) :
	"""
	Convolves/rebins the entries of a spectral table (like convolve_rebin() but with more Table support).
	The "other" list contains names of numerical data columns that should also be convolved/rebinned;
	without "other", rebinning will result in the loss of table columns whose interpolated.
	Returns a new table.
	"""
	assert wavcol is not None
	assert flxcol is not None
	t = Table(tab)
	for col in [wavcol,flxcol,errcol] :
		if (col is not None) and (col not in t.colnames) :
			logging.error ('{0} not a table column label'.format(col))
			return None

	# PRESENT MEDIAN DISPERSION
	dispersion = np.median(np.diff(t[wavcol]))

	# CONVOLVE?
	if dlambda is not None :
		resol = dlambda/dispersion		# WAVELENGTH -> PIXELS
		cols = [flxcol,errcol]
		if other is not None : cols += other
		for col in cols :
			if col is not None :
				t[col] = gaussian_filter1d (t[col], resol)

	# REBIN?
	if wave_out is not None :
		outtab = Table()
		outtab.meta = tab.meta
		err_in = None
		if errcol is not None : err_in = t[errcol]
		if model == 'histogram' :
			outtab[flxcol],err = hist_rebin (t[wavcol],t[flxcol],err_in,wave_out)
			outtab[flxcol].units = 'nm'
			if errcol is not None :
				outtab[errcol] = err
				outtab[errcol].units = 'nm'
		elif model == 'interpolate' :
			f = np.interp (wave_out,t[wavcol],t[flxcol])
			outtab[flxcol] = Column (f, unit=t[flxcol].unit, description='interpolated flux')
			if errcol is not None :
				f = np.interp (wave_out,t[wavcol],t[errcol])
				outtab[errcol] = Column (f, unit=t[flxcol].unit, description='interpolated flux error')
		if (pixcol is not None) and (pixcol in t.colnames) :
			p = np.interp (wave_out,t[wavcol],t[pixcol])
			outtab[pixcol] = Column (p, unit='pix', description='interpolated pixel')
		if other is not None :
			for col in other :
				if col is not None :
					x = np.interp (wave_out,wave_in,t[col])
					outtab[col] = Column (x,  unit=t[col].unit, description='interpolated')
	else :
		outtab = t

	# RETURN NEW TABLE
	return outtab

def convolve_rebin (wave_in=None, flux_in=None, err_in=None, dlambda=None, wave_out=None, model='interpolate') :
	"""
	Convolves the input spectrum to a given dlambda (as opposed to a given R=lambda/dlambda!) and
	performs a rebinning to a new wavelength sampling.
	Returns one or two arrays.
	"""
	if wave_in is None and flux_in is None :
		raise ValueError ('No input data given!')

	# PRESENT MEDIAN DISPERSION
	dispersion = np.median(np.diff(wave_in))

	# CONVOLVE TO TARGET WAVELENGTH RESOLUTION
	err = err_in
	if dlambda is not None :
		resol = dlambda/dispersion		# WAVELENGTH -> PIXELS
		conv = gaussian_filter1d (flux_in, resol)
		if err_in is not None :
			err = gaussian_filter1d (err_in, resol)
	else :
		conv = flux_in
	err_out = err

	# REBIN TO THE INPUT WAVELENGTH ARRAY
	if wave_out is not None :
		if model == 'histogram' :
			bconv,err_out = hist_rebin (wave_in,conv,err,wave_out)
		elif model == 'interpolate' :
			bconv = np.interp (wave_out,wave_in,conv)
			if err_in is not None :
				err_out = np.interp (wave_out,wave_in,err_in)
		else :
			raise NotImplementedError ('{0} is not an implemented convolve model '.format(model))
		if err_out is not None :
			return bconv,err_out
		else :
			return bconv
	else :
		if err_out is not None :
			return conv,err
		else :
			return conv

def Gaussian1D (x,a,b,c,d) :
	"""
	Simple 1-D Gaussian function
	"""
	return a+b*np.exp(-(x-c)**2/d**2)

def merge_dictionaries (dict1,dict2) :
	"""
	Merge the secondary dictionary dict2 into the prime dictionary dict1 if there's something new to merge.
	"""
	assert isinstance(dict1,dict)
	if dict2 is None : return
	assert isinstance(dict2,dict)

	# FOR EVERYTHING IN SECONDARY DICTIONARY...
	for key2,val2 in dict2.items() :

		# IF NOT PRESENT, SIMPLY ADD TO PRIMARY
		if key2 not in dict1 or dict1[key2] is None :
			try :
				dict1[key2] = val2
			except TypeError as e :
				logging.error (str(e)+'\nkey2={0}\nval2={1}\ndict1={2}'.format(key2,val2,dict1))
				raise TypeError ('problem in merge_dictionaries')
		# IF PRESENT AND A DICTIONARY, USE A RECURSIVE MERGE
		elif isinstance(val2,dict) :
			merge_dictionaries (dict1[key2],val2)

def header2config (header, keywkeys) :
	"""
	Convert a pyFU-compatible header into a pyFU configuration dictionary using
	a dictionary of FITS keyword keys, e.g.

		{'index': 'IF-INDEX', 'xpos-format':'IF-XPFMT'}

	The particular configuration parameters are placed at the highest level in
	the configuration dictionary; the collection of parameters are placed in their
	corresponding sub-dictionaries.
	"""
	difu = {}
	dpos = {}
	damp = {}
	dtrace = {'fit': {'amplitudes':damp, 'positions':dpos}}
	dformats = {}
	config = {'ifu':difu,'trace':dtrace,'formats':dformats}
	indices = {}
	labels = {}

	# GET INDEX OF PARTICULAR FIBRE
	idx = None
	if 'index' in keywkeys and keywkeys['index'][0] in header :
		idx = header[keywkeys['index'][0]]
		config['index'] = idx
	elif 'trace_index' in keywkeys and keywkeys['trace_index'][0] in header :
		idx = header[keywkeys['trace_index'][0]]
		config['index'] = idx
	# GET INDICES OF ALL FIBRES
	elif 'index_format' in keywkeys and keywkeys['index_format'][0] in header :
		fmtkey = keywkeys['index_format'][0]
		fmt = header[fmtkey]
		for i in range(999) :
			key = fmt.format(i)
			if key in header :
				indices[i] = header[key]

	# GET LABEL OF PARTICULAR FIBRE
	flabel = None
	if 'label' in keywkeys and keywkeys['label'][0] in header :
		if idx is not None :
			flabel = header[keywkeys['label'][0]]
			config['label'] = flabel
	# GET LABELS OF ALL FIBRES
	elif 'label_format' in keywkeys and keywkeys['label_format'][0] in header :
		fmtkey = keywkeys['label_format'][0]
		fmt = header[fmtkey]
		for i in range(999) :
			key = fmt.format(i)
			if key in header :
				labels[i] = header[key]

	# RE-CONSTRUCT SLIT
	if len(indices) > 0 :
		l = list(indices.keys())
		l.sort()
		slit = []
		for i in l :
			if i in indices and i in labels :
				slit.append(labels[i])
		difu['slit'] = slit

	# GET POSITION OF PARTICULAR FIBRE
	pos = None
	if 'xpos' in keywkeys and 'ypos' in keywkeys and keywkeys['xpos'][0] in header and keywkeys['ypos'][0] in header :
		if idx is not None :
			xpos = header[keywkeys['xpos'][0]]
			ypos = header[keywkeys['ypos'][0]]
			config['xpos'] = xpos
			config['ypos'] = ypos
			pos = [xpos,ypos]
	# GET POSITIONS OF ALL FIBRES
	elif 'xpos_format' in keywkeys and 'ypos_format' in keywkeys and keywkeys['xpos_format'][0] in header and keywkeys['ypos_format'][0] in header :
		xfmtkey = keywkeys['xpos_format'][0]
		yfmtkey = keywkeys['ypos_format'][0]
		xfmt = header[xfmtkey]
		yfmt = header[yfmtkey]
		info = config['ifu']
		d = {}
		for i in range(999) :
			xkey = xfmt.format(i)
			ykey = yfmt.format(i)
			if xkey in header and ykey in header :
				x = header[xkey]
				y = header[ykey]
				pos = [x,y]
				if i in labels :
					d[labels[i]] = pos
		difu['focal_positions'] = d
					
	# GET SIGMA OF PARTICULAR FIBRE
	d_in = None
	if 'sigma' in keywkeys and keywkeys['sigma'][0] in header :
		key = keywkeys['sigma'][0]
		sigma = header[key]
		config['sigma'] = sigma
	# GET SIGMAS OF ALL FIBRES
	elif 'sigma_format' in keywkeys and keywkeys['sigma_format'][0] in header :
		fmtkey = keywkeys['sigma_format'][0]
		fmt = header[fmtkey]
		d = {}
		for i in range(999) :
			key = fmt.format(i)
			if key in header :
				sigma = header[key]
				if i in labels :
					d[labels[i]] = sigma
		dtrace['sigmas'] = d

	# GET DIAMETER OF PARTICULAR FIBRE
	d_in = None
	if 'inner_diameter' in keywkeys and keywkeys['inner_diameter'][0] in header :
		key = keywkeys['inner_diameter'][0]
		d_in = header[key]
		config['inner_diameter'] = d_in
	# GET DIAMETERS OF ALL FIBRES
	elif 'inner_diameter_format' in keywkeys and keywkeys['inner_diameter_format'][0] in header :
		fmtkey = keywkeys['inner_diameter_format'][0]
		fmt = header[fmtkey]
		d = {}
		for i in range(999) :
			key = fmt.format(i)
			if key in header :
				d_in = header[key]
				if i in labels :
					d[labels[i]] = d_in
		difu['inner_diameters'] = d

	d_out = None
	if 'outer_diameter' in keywkeys and keywkeys['outer_diameter'][0] in header :
		key = keywkeys['outer_diameter'][0]
		d_out = header[key]
		config['outer_diameter'] = d_in
	elif 'outer_diameter_format' in keywkeys and keywkeys['outer_diameter_format'][0] in header :
		fmtkey = keywkeys['outer_diameter_format'][0]
		fmt = header[fmtkey]
		d = {}
		for i in range(999) :
			key = fmt.format(i)
			if key in header :
				d_out = header[key]
				if i in labels :
					d[labels[i]] = d_out
		difu['outer_diameters'] = d

	# GET TRACE COEFFICIENTS
	if 'trace_format' in keywkeys and keywkeys['trace_format'][0] in header :
		fmt = header[keywkeys['trace_format'][0]]

		# ... FOR NAMED FIBRE
		if idx is not None and flabel is not None :
			p = []
			for k in range(9) :
				key = fmt.format(idx,k)
				if key in header :
					p.append(header[key])
			if len(p) == 0 :
				logging.error ('could not extract position coefficients from header for #{0} = {1}'.format(idx,neame))
			else :
				dpos['by_label'] = {flabel:p}
				dpos['by_index'] = {idx:p}
		# ... FOR ALL FIBRES
		elif len(indices) > 0 :
			dpos['by_label'] = {}
			dpos['by_index'] = {}
			for i in indices :
				if i in labels :
					label = labels[i]
					p = []
					for k in range(9) :
						key = fmt.format(i,k)
						if key in header :
							p.append(header[key])
					if len(p) == 0 :
						logging.warning ('could not extract position coefficients from header for #{0} = {1}'.format(i,label))
					else :
						if label is not None :
							dpos['by_label'][label] = p
							dpos['by_index'][i] = p
				else :
					logging.error ('index #{0} is not labelled in header!')
	else :
		logging.error ('no "trace_format" equivalent keyword in header')

	# GET AMPLITUDE COEFFICIENTS
	if 'ampl_format' in keywkeys and keywkeys['ampl_format'][0] in header :
		fmt = header[keywkeys['ampl_format'][0]]

		# ... FOR NAMED FIBRE
		if idx is not None and flabel is not None :
			p = []
			for k in range(9) :
				key = fmt.format(idx,k)
				if key in header :
					p.append(header[key])
			if len(p) == 0 :
				logging.error ('could not extract ampl coefficients from header for #{0} = {1}'.format(idx,neame))
			else :
				damp['by_label'] = {flabel:p}
				damp['by_index'] = {idx:p}
		# ... FOR ALL FIBRES
		elif len(indices) > 0 :
			damp['by_label'] = {}
			damp['by_index'] = {}
			for i in indices :
				if i in labels :
					label = labels[i]
					p = []
					for k in range(9) :
						key = fmt.format(i,k)
						if key in header :
							p.append(header[key])
					if len(p) == 0 :
						logging.warning ('could not ampl coefficients from header for #{0} = {1}'.format(i,label))
					else :
						if label is not None :
							damp['by_label'][label] = p
							damp['by_index'][i] = p
				else :
					logging.error ('index #{0} is not labelled in header!')
	else :
		logging.error ('no "ampl_format" equivalent keyword in header')

	# RETURN CONFIGURATION DICTIONARY
	return config

def copy_ifu_header (hdr_in,hdr_out, idx=None, keywords=None) -> bool :
	"""
	Copies non-fundamental header entries from one FITS header to another.
	If idx and keywords are not None, then only the pyFU metadata for that spectrum are transferred.
	The spectrum index is assumed to be placed before the polynomial order in the coded info.
	The "keywords" dictionary is assumed to have the structure {key: (keyword,comment), ...}.
	"""
	if hdr_in is None :
		return False
	if hdr_out is None :
		hdr_out = {}

	# PREPARE A DICTIONARY OF INDEXED KEYWORDS TO INCLUDE
	keep = {}
	if idx is not None and keywords is not None :
		if 'trace_format' in keywords :			# FITS KEYWORD AVAILABLE?
			coefkey = keywords['trace_format'][0]
			if coefkey in hdr_in :				# KEYWORD IN HEADER?
				fmt = hdr_in[coefkey]
				val = fmt.format(idx,0)			# DUMMY 2ND INDEX
				result = sscanf (fmt,val)		# RE-EXTRACTED INDEX AS SEQUENCE (idx)
				keep[fmt] = result
		if 'xpos_format' in keywords :
			xposkey = keywords['xpos_format'][0]
			if xposkey in hdr_in :
				fmt = hdr_in[xposkey]
				val = fmt.format(idx)
				result = sscanf (fmt,val)
				keep[fmt] = result
		if 'ypos_format' in keywords :
			yposkey = keywords['ypos_format'][0]
			if yposkey in hdr_in :
				fmt = hdr_in[yposkey]
				val = fmt.format(idx)
				result = sscanf (fmt,val)
				keep[fmt] = result

	ignore = ['SIMPLE','BITPIX','XTENSION','NAXIS','CTYPE','CUNIT','CPIX','END','PCOUNT','TFIELDS','TFORM','TTYPE','TUNIT']

	# CHECK ALL KEYWORDS IN HEADER
	for key,val in hdr_in.items() :
		ok = True

		# CHECK ALL IGNORED KEYWORDS
		for ikey in ignore :
			if key.startswith (ikey) :
				ok = False

		# CHECK TO SEE IF INDEXED KEYWORDS MATCH
		for fmt,want in keep.items() :
			try :
				result = sscanf (fmt,key)
				same = result[0] == want[0]
				if not same :		# CLOSE, BUT NO CIGAR, SO NOT OK
					ok = False
			except :				# NOT EVEN CLOSE, SO OK
				pass
		if ok :
			hdr_out[key] = val

	return True

def UTC_now () :
	return datetime.datetime.utcnow()

def hist_integral (x,y,yerr,wx,x_1,x_2, normalize=False, strict=False) :
	"""
	\int_x1^x2 y dx for previously binned data (like CCD pixels).
	x are the positions of the x-bin centres.
	wx is the array of widths of the x-bins, where the bins are assumed
	to touch, i.e.  x[i]+0.5*wx[i] = x[i+1]-0.5*wx[i+1].
	"""
	# TRIVIAL VALIDITY TEST
	if strict and (x1 < x[0] or x2 <= x[0] or x1 >= x[-1] or x2 > x[-1]) :
		return np.nan,np.nan

	x1 = x_1+0.
	x2 = x_2+0.
	if x1 < x[0]  : x1 = x[0]
	if x2 > x[-1] : x2 = x[-1]
	if wx is None :
		wx = np.concatenate((np.diff(x),[x[-1]-x[-2]]))	# SAME SIZE AS x

	n = len(x)
	yint = 0.0
	if yerr is None :
		errors = False
		err = None
	else :
		errors = True
		err = 0.0

	# GET INDEX RANGE
	i1 = np.searchsorted (x,x1)	# INDEX OF DATA x-VALUE >= x1
	i2 = np.searchsorted (x,x2) 

	# EVERYTHING WITHIN A SINGLE BIN
	if i1 == i2 :
		ilow = 0
		ihi  = 0
		xmid = x[i2]-0.5*wx[i2]	# MID-POINT

		if x1 < xmid :	# LOWER CONTRIBUTION
			z1 = (min(xmid,x2)-x1)/wx[i2-1]
			dyint = y[i2-1]*max(0.,z1)*wx[i2-1]
			if errors :
				err += yerr[i2-1]**2*max(0.,z1)**2
			yint += dyint

		if x2 > xmid :	# UPPER CONTRIBUTION
			z2 = (x2-max(xmid,x1))/wx[i2]
			dyint = y[i2]*max(0.,z2)*wx[i2]
			if errors :
				err += yerr[i2]**2*max(0.,z2)**2
			yint += dyint

	# SPREAD OVER LOWER FRACTIONAL BIN, MIDDLE BINS, AND UPPER FRACTIONAL BIN
	else :

		# LOWER FRACTION
		xmid = x[i1]+0.5*wx[i1]

		if x1 < xmid :	# FULL CONTRIBUTION FROM UPPER FRACTIONAL BIN
			z1 = (xmid-x1)/wx[i1]
			dyint = y[i1]*max(0.,z1)*wx[i1]
			if errors :
				err += yerr[i1]**2*max(0.,z1)**2
			yint += dyint

			ilow = i1+1

		else :	# NO CONTRIBUTION FROM LOWER FRACTIONAL BIN
			z2 = (x[i1+1]-x1)/wx[i1+1]
			dyint = y[i1+1]*max(0.,z2)*wx[i1+1]
			if errors :
				err += yerr[i1+1]**2*max(0.,z2)**2
			yint += dyint

			ilow = i1+2

		# UPPER FRACTION
		xmid = x[i2]-0.5*wx[i2]

		if x2 < xmid :	# NO CONTRIBUTION FROM UPPER FRACTIONAL BIN
			z1 = (x2-(x[i2-1]-0.5*wx[i2-1]))/wx[i2-1]
			dyint = y[i2-1]*max(0.,z1)*wx[i2-1]
			if errors :
				err += yerr[i2-1]**2*max(0.,z1)**2
			yint += dyint

			ihi = i2-2

		else :			# FULL CONTRIBUTION FROM LOWER FRACTIONAL BIN
			z2 = (x2-xmid)/wx[i2]
			dyint = y[i2]*max(0.,z2)*wx[i2]
			if errors :
				err += yerr[i2]**2*max(0.,z2)**2
			yint += dyint

			ihi = i2-1

		# MIDDLE FRACTION
		dyint = 0.
		for i in range(ilow,ihi+1,1) :
			dyint += y[i]*wx[i]
			if errors :
				err += yerr[i2]**2
		yint += dyint

	if errors :
		err = np.sqrt(err)
	if normalize :
		yint /= (x2-x1)
		if errors :
			err /= (x2-x1)
	return yint,err

def hist_rebin (x, y, yerr, xref, debug=False, normalize=False) :
	"""
	Rebins the (x,y,err_y) data in a table to match the x-scale of xref.
	The x-point are assumed to represent the centers of spectral bins,
	so the rebinning includes the integration of each pixel from one side to the other.
	If yerr is not None, the binned errors are also computed and returned.

	Returns y_rebin,err_y_rebin
	"""
	nx   = len(x)
	nref = len(xref)
	ybin = xref-xref
	if yerr is not None :
		err_ybin = ybin-ybin
	else :
		err_ybin = None

	# GET RIGHT-HANDED TOTAL BIN SIZES
	dx = np.concatenate((np.diff(x),[x[-1]-x[-2]]))			# SAME SIZE AS x

	# FOR ALL xref BINS
	dxref = xref[1]-xref[0]		# STARTING BIN
	for i in range (nref) :
		x1ref = xref[i]-0.5*dxref
		x2ref = x1ref+dxref
		y_int,err_int = hist_integral (x,y,yerr,dx,x1ref,x2ref,normalize=normalize)
		ybin[i] = y_int
		if yerr is not None :
			err_ybin[i] = err_int

		if i < nref-1 :			# GET NEXT BIN
			dxref = 0.5*(dxref+xref[i+1]-xref[i])

	# RETURN RESULT
	return ybin,err_ybin

def write_tables (tables, pathname='./', header=None, overwrite=True, 
						fmt=None, keywords=None) :
	"""
	Writes a list of Tables to some supported formats.
	If the format string is not given, then "pathname" is the file name.
	If the format string is given, the spectra are written to multiple
	files, e.g. 'spectrum_{0:00d}.txt', and the pathname is the directory used.
	The FITS "header" is used when writing to a FITS binary table file: the
	header is then included in the primary HDU.
	If "keywords" is not None, then index-based pyFU keywords are not copied
	from the header.
	"""
	if fmt is not None :
		name,ftype = os.path.splitext(fmt)
	else :
		name,ftype = os.path.splitext(pathname)
	if ftype not in PYFU_EXTENSIONS :
		logging.error ('unknown file format for {0} or missing prefix format string: {1}'.format(pathname,ftype))
		return

	if ftype == '.fit' or '.fits' :
		# SINGLE FITS FILE
		if fmt is None :
			phdu = fits.PrimaryHDU()
			hs = [phdu]
			if header is not None :
				copy_ifu_header (header,phdu.header,idx=0,keywords=keywords)
			for i in range(len(tables)) :
				t = tables[i]
				thdu = fits.table_to_hdu(t)
				hs.append (thdu)
				logging.debug ('appending spectrum #{0} to HDU list of {1}...'.format(i+1,pathname))

			hdus = fits.HDUList (hs)
			hdus.writeto (pathname,overwrite=overwrite)


		# MULTIPLE FITS FILES
		else :
			for i in range (len(tables)) :
				t = tables[i]
				name = pathname
				if not pathname.endswith('/') :
					name += '/'
				name += fmt.format(i)
				if hdu is not None :
					copy_ifu_header (hdu.header,t.meta)
				logging.debug ('writing spectrum #{0} to {1}...'.format(i+1,name))
				t.write (name, format='fits',overwrite=overwrite)

	else :
		# SINGLE TEXT FILE
		if fmt is None :
			tab = Table()
			for i in range(len(tables)) :
				t = tables[i]
				for key in t.colnames() :
					newkey = key+'__{0:00d}'.format(i)
				tab[newkey] = t[key]
				logging.debug ('appending spectrum #{0} to {1}...'.format(i+1,pathname))
				t.write (pathname, format=PYFU_EXTENSIONS[ftype],overwrite=overwrite)

		# MULTIPLE TEXT FILES
		else :
			for i in range (len(tables)) :
				t = tables[i]
				name = pathname
				if not pathname.endswith('/') :
					name += '/'
				name += fmt.format(i)
				logging.info ('writing spectrum #{0} to {1}...'.format(i+1,name))
				t.write (name, format=PYFU_EXTENSIONS[ftype],overwrite=overwrite)

def spectrum2Table (hdu) :
	"""
	Reads a 1-D spectrum from a FITS HDU into a Table.
	The wavelength scale is hopefully in a simple, linear WCS!
	"""
	hdr = hdu.header
	if hdr['NAXIS'] != 1 :
		logging.error ('spectrum2Table can only construct 1-D tables!')
		return None
	nw = hdr['NAXIS1']

	# GET FLUX
	bscale = 1.0
	bzero  = 0.0
	"""
	if 'BSCALE' in hdr and 'BZERO' in hdr :
		bscale = hdr['BSCALE']
		bzero  = hdr['BZERO']
	"""
	flux = hdu.data*bscale+bzero

	# GET WAVELENGTH
	if 'CRVAL1' in hdr and 'CDELT1' in hdr :		# SIMPLE WCS
		crpix1 = 1
		if 'CRPIX1' in hdr :
			crpix1 = hdr['CRPIX1']
		pixl = np.arange(nw)
		w0 = hdr['CRVAL1']
		dwdx = hdr['CDELT1']
		wave = w0+dwdx*(pixl+1-(crpix1-1))
	else :
		logging.error ('cannot reconstruct wavelength from FITS header')
		return None

	# GET UNITS
	if 'CUNIT1' in hdr :
		cunit1 = hdr['CUNIT1']
	else :							# ASSUME ASTRONOMERS USE ANGSTROMS
		cunit1 = 'nm'
		wave /= 10.

	# CONSTRUCT Table
	t = Table()
	t['wavelength'] = Column(wave,unit=cunit1, description='Wavelength')
	t['flux']       = Column(flux,unit='unknown', description='Flux')
	t.meta = hdr
	return t

def read_tables (pathname=None, fmt=None) :
	"""
	Reads spectra as a list of Tables from one or more files.
	If no format string is given, then the path name is the name of the file.
	If the format string is given, the spectra are read from multiple
	files, e.g. 'spectrum_*.txt', and the pathname is the directory used.
	returns a list of tables and the primary HDU header, if available.
	"""
	tables = []
	header = None

	if fmt is not None :
		name,ftype = os.path.splitext(fmt)
		fullname = pathname+'/'+fmt
	else :
		name,ftype = os.path.splitext(pathname)
		fullname = pathname
	if ftype not in PYFU_EXTENSIONS :
		logging.error ('unknown file format for {0} or missing prefix format string: {1}'.format(pathname,ftype))
		return None

	files = get_list_of_paths_and_filenames (fullname)
	for f,name in files :
		if ftype == '.fit' or '.fits' :
			hdus = fits.open (f)
			header = hdus[0].header
			for i in range(0,len(hdus)) :
				hdu = hdus[i]
				hdr = hdu.header
				# BINARY TABLE
				if 'XTENSION' in hdr and hdr['XTENSION'] == 'BINTABLE' :
					header = hdr
					logging.debug ('extracting tabular spectrum from {0},HDU#{1} ...'.format(f,i))
					t = Table.read (hdus,hdu=i)
					t.meta['FILENAME'] = name+'#{0}'.format(i+1)
					tables.append(t)
				# 1-D "IMAGE"
				elif 'NAXIS1' in hdr :
					logging.debug ('extracting image spectrum from {0} ...'.format(name))
					t = spectrum2Table (hdu)
					if t is not None :
						t.meta['FILENAME'] = name
						tables.append(t)

		# READ CONGLOMERATED TABLE
		elif fmt is None :
			logging.info ('reading conglomerated ascii spectra {0} ...'.format(f))
			try :
				t = Table.read (f, format=PYFU_EXTENSIONS[ftype])
				# SEPARATE INTO INDIVIDUAL TABLES
				tabs = {}
				for key in t.colnames() :
					if '__' in key :
						try :
							i = key.rindex('_')
							idx = int(key[i+1:])
							oldkey = key[:i-1]
							if not idx in tabs :
								tabs[idx] = Table()
								tabs.meta['FILENAME'] = +name+'#{0}'.format(i)
								tables.append(tabs[idx])
							tabs[idx][oldkey] = t[key]
						except ValueError :
							pass
			except Exception as e :
				logging.info (str(e))

		# MULTIPLE TEXT FILES
		else :
			logging.info ('reading ascii spectrum {0} ...'.format(f))
			t = Table.read (pathname+'/'+f,format=extensions[ftype])
			if 'FILENAME' not in t.meta :
				t.meta['FILENAME'] = f
			tables.append(t)

	# RETURN RESULT
	return tables,header

def get_image_limits (hdu,mode='number') :
	"""
	Get size and intensity limits from an image stored in a FITS HDU (python coordinates!).
	"""
	hdr = hdu.header
	data = hdu.data
	xmin = 0
	xmax = hdr['NAXIS1']-1
	ymin = 0
	ymax = hdr['NAXIS2']-1
	zmin = np.nanmin(data)
	zmax = np.nanmax(data)

	if mode == 'outside' :	# INCLUDE SIZE OF PIXELS, E.G. FOR pyplot.imshow()
		xmin -= 0.5
		xmax += 0.5
		ymin -= 0.5
		ymax += 0.5

	return xmin,xmax,ymin,ymax,zmin,zmax

def centroid1D (yarr,pos,width, get_sigma=False, get_fwhm=False, subt_bkg=True) :
	"""
	Get the centroid of 1-D x and y sub-arrays at a particular position and window width.
	"""
	w = int(width-1)//2
	i1 = int(pos-w)
	if i1 < 0 : i1=0
	i2 = int(i1+width-1)
	if i2 >= len(yarr) : i2=len(yarr)-1
	i1 = int(i2-width+1)

	n = len(yarr)
	xarr = np.arange(n)
	x = xarr[i1:i2+1]
	y = yarr[i1:i2+1]
	if subt_bkg :
		bkg = np.min(y)
	else :
		bkg = 0.
	cntrd = np.sum(x*(y-bkg))/np.sum(y-bkg)
	width = 3.*np.sqrt(np.abs(np.sum((y-bkg)*(x-cntrd)**2)/np.sum(y-bkg)))
	i = int(cntrd+0.5)
	mx = yarr[i]
	i1,i2 = i-1,i+1
	while i1 > 0   and yarr[i1] > 0.5*mx : i1 -= 1
	while i2 < n-1 and yarr[i2] > 0.5*mx : i2 += 1
	x1 = (0.5*mx-yarr[i1]*(i1+1)+yarr[i1+1]*i1)/(yarr[i1+1]-yarr[i1])
	x2 = (0.5*mx-yarr[i2-1]*i2+yarr[i2]*(i2-1))/(yarr[i2]-yarr[i2-1])
	fwhm = x2-x1
	if np.abs(fwhm-(i2-i1)) > 1 :
		fwhm = i2-i1

	if not get_sigma and not get_fwhm :
		return cntrd
	elif get_sigma and not get_fwhm :
		return cntrd,width
	elif get_fwhm and not get_sigma :
		return cntrd,fwhm
	else :
		return cntrd,width,fwhm

def centroid (x,y,m, subtract_median=False, subtract_min=False) :
	"""
	Returns the centroid and a measure of the width of an array y(x)
	for m values around the peak.
	If "subtract_median" is True, then the median value is first subtracted.
	"""
	n = len(x)
	sumxy = 0.0
	sumy  = 0.0
	ysub   = 0.0
	if subtract_median :
		ysub = np.median(y)
	elif subtract_min :
		ysub = np.min(y)
	peak = np.argmax(y)	# INDEX OF HIGHEST PEAK
	for i in range(peak-m//2,peak+m//2) :
		if i >= 0 and i < n :
			sumxy += (y[i]-ysub)*x[i]
			sumy  += y[i]-ysub
	if np.isnan(sumy) or sumy == 0.0 :
		return np.nan,np.nan

	x0 = sumxy/sumy
	sumydx2 = 0.0
	for i in range(peak-m//2,peak+m//2+1) :
		if i >= 0 and i < n :
			dx = x[i]-x0
			sumydx2 += (y[i]-ysub)*dx*dx
	w = 3.*np.sqrt(np.abs(sumydx2/sumy))
	return x0,w

def read_spectrum (filename, hdu=1) :
	"""
	Extracts a spectrum table from a FITS or ascii table.
	"""
	if filename.endswith ('.csv') :
		table = Table.read (filename, format='ascii.csv')
		return table	# [wcol],table[fcol],table.meta
	elif filename.endswith ('.txt') or filename.endswith ('.dat') :
		table = Table.read (filename,format='ascii.tab')
		return table
	elif filename.endswith ('.fits') :
		hdus = fits.open (filename)
		if len(hdus) == 1 :
			table = spectrum2Table (hdus[0])
		else :
			table = Table.read (hdus, hdu=hdu)
		return table
	else :
		logging.error ('Unable to read {0}'.format(filename))
		sys.exit(1)

def write_spectrum (tab,filename) :
	"""
	Writes a spectrum table to a FITS or ascii table.
	"""
	if filename.endswith ('.csv') :
		table.write (filename, format='ascii.csv')
	elif filename.endswith ('.txt') or filename.endswith ('.dat') :
		table.write (filename,format='ascii.tab')
	elif filename.endswith ('.fits') :
		table.write (filename,format='fits')	
	else :
		raise Exception ('Unable to write {0}'.format(filename))

def cubic (x,a,b,c) :
	return a+b*x+c*x**2+d*x**3
def poly (x,a,b,c,d) :
	return a+b*x+c*x**2+d*x**3
def line (x,a,b) :
	return a+b*x

def cubic_equation (a,b,c,d) :
	"""
	Solves the cubic equation
		a*x^3+b*x^2+c*x+d = 0
	by reducing to the depressed cubic
		t^3+p*t+q=0
		x = t-b/(3*a)
		p = (3*a*c-b^2)/(3*a^2)
		q = (2*b^3-9*a*b*c+27*a^2*d)/(27*a^3)
	which, using Vieta's substitution
		t = w-p/(3*w)
	becomes
		w^3+q-p**3/(27*w^3) = 0
	or the quadratic equation
		(w^3)^2+q*(w^3)-p^3/27. = 0
	which has the roots
		w1
	"""
	raise NotImplementedException ('cubic_equation')

def parse_arguments (arguments, readme=None, config=None, parser=None) :
	"""
	Extends argparse command line parsing with the possibility of using a default YAML dictionary.
	The input dictionary "arguments" contains

		keyword: {'path':dict_path,'default':actual_default,'dshow':parsed_default,'flg':one_char_flag,'type':type,'help':help_text}

	dict_path is a string encoding where in the dictionary the value should be placed, e.g.

		'path':'scatter:model:polynomial:order'

	means that the argument should be placed {'scatter':{'model':{'polynomial':{'order':HERE}}}}.  If no path is given, then the
	value is placed at the highest level of the configuration dictionary.

	The 'default' defaults are those ultimately used; 'dshow' is used so that the YAML dictionary can contain the
	true defaults before the command line input is parsed, i.e. the defaults automatically shown in the help text
	are not actually given to argparse.

	Returns classic argparse argument dictionary, the updated dictionary, and the infokey sub-dictionary, if used.
	"""
	if config is None :
		config = {}

	# ---- CREATE PARSER AND PARSE COMMAND LINE	
	if parser is None :
		parser = argparse.ArgumentParser (readme)
	for arg in arguments :
		udict = arguments[arg]
		for key in ['default','dshow','flg','type','help'] :
			if key not in udict :
				raise ValueError ('user dictionary {0} does not contain {1}!'.format(str(udict),key))
		flag = '--{0}'.format(arg)
		help = '{0} (default {1})'.format(udict['help'],udict['dshow'])
		if udict['type'] is bool :
			if udict['dshow'] :
				parser.add_argument (flag,udict['flg'],default=udict['default'],action='store_false',help=udict['help'])
			else :
				parser.add_argument (flag,udict['flg'],default=udict['default'],action='store_true',help=udict['help'])
		elif udict['type'] is list :
			parser.add_argument (flag,udict['flg'],default=udict['default'],type=str,help=udict['help'])
		else :
			parser.add_argument (flag,udict['flg'],default=udict['default'],type=udict['type'],help=udict['help'])
	args = parser.parse_args ()

	# ---- UPDATE DICTIONARY WITH YAML FILE FOR DEFAULTS
	if 'yaml' in arguments and args.yaml is not None :
		with open(args.yaml) as stream :
			d = yaml.safe_load(stream)
		merge_dictionaries (config,d)

	# ---- UPDATE CONFIGURATION WITH COMMAND LINE INPUT
	adict = args.__dict__	# DICTIONARY OF ARGUMENTS FROM argparse
	for arg in adict :		# AS IN args.{arg}

		if adict[arg] is not None :	# IF THERE IS CONTENT TO BE SAVED...
			udict = arguments[arg]	# DICTIONARY OF COMMAND LINE PARAMETER INFO FROM USER
			loc = config
			key = arg

			# PARSE DICTIONARY PATH TO GET FINAL DESTINATION
			if 'path' in udict and udict['path'] is not None :
				levels = udict['path'].split(':')
				for level in levels[:-1] :	# FOR ALL BUT LAST PATH ENTRY
					if level not in loc :	# ADD MISSING CONFIGURATION ENTRIES
						loc[level] = {}
					loc = loc[level]
				if levels[-1] != '' :
					key = levels[-1]

			# CONVERT LISTS FROM str TO int OR float
			if udict['type'] is list :
				l = adict[arg].split(',')	# SPLIT CONTENTS OF args.* IN A LIST
				try :
					if '.' in adict[arg] :	# PROBABLY CONTAINS float
						for i in range(len(l)) :
							l[i] = float(l[i])
					else :					# PROBABLY CONTAINS int
						for i in range(len(l)) :
							l[i] = int(l[i])
				except :					# OR GO BACK TO SIMPLE STRINGS
					l = adict[arg].split(',')
				loc[key] = l

			# OR SIMPLY TRANSFER TO DICTIONARY
			else :
				loc[key] = adict[arg]

	# ---- RESULTS
	return args,config

def vectorize (func,x,*args) :
	"""
	Runs func(x,*args) on an np.array "x" when it normally wouldn't work.
	"""
	if isinstance(x,float) or isinstance (x,int) :
		return func (x, *args)
	elif isinstance (x,np.ndarray) or isinstance (x,list) :
		n = len(x)
		f = np.zeros(n)
		for i in range(n) :
			f[i] = func (x[i], *args)
		return f
	else :
		raise NotImplementedError ('cannot vectorize {0}'.format(str(type(x))))

def stripped_format (fmt) :
	"""
	Takes a perfectly reasonable python format string and removes the size info so that
	parse.parse can read values using it (like C's "sscanf").
	Any reasonable language having str.format would have the reverse operation..... :-(

	Example:  "COE{0:03d}_{1:1d}"...
	"""
	parts = fmt.split('{')	# e.g. ["COE","0:03d}_","1:1d}"]
	if len(parts) == 1 :
		raise ValueError ('string is not a format: {0}'.format(fmt))
	f = parts[0]
	for i in range(1,len(parts)) :
		if '}' not in parts[i] :	# WHOOPS!
			raise ValueError ('no matching {} in format!')
		things = parts[i].split('}')		# e.g. ["0:03d","_"]
		stuff = things[0].split(':')		# e.g. ["0","03d"]
		f += '{:'+stuff[1]+'}'
		if len(things) == 2 :
			f += things[1]
	return f

def sscanf (fmt,s) :
	"""
	Rough equivalent of C's sscanf using parse.parse.
	Returns a list of values extracted using a format string.
	"""
	return parse.parse (stripped_format(fmt),s).fixed

def get_list_of_paths_and_filenames (path_pattern, mode='both') :
	"""
	Returns a list of paths, filenames, or both using the given pathname and pattern.
	The pattern should include a "*" if it refers to multiple files,
	e.g. "/mydir/x*.fits" would enable getting a list like

		[("/mydir/x01.fits","x01.fits"),("/mydir/x02.fits","x0002.fits"), ...]
	"""
	files = []
	things = []

	# EXTRACT DIRECTORY NAME AND FILE PATTERN
	i = path_pattern.rindex('/')
	pathname = path_pattern[:i]
	pattern = path_pattern[i+1:]

	# GET LIST OF ALL FILES IN THAT DIRECTORY
	for (dirpath,dirnames,filenames) in os.walk (pathname) :
		things.extend(filenames)
		break	# ONLY GO ONE PATH LEVEL

	# SEPARATE PATTERN INTO BEGINNING AND ENDING PARTS
	if pattern is None :
		parts = None
	else :
		parts = pattern.split('*')

	# FOR ALL FILES, SEE IF THEY MATCH THE PATTERN
	for f in things :
		if parts is None :
			name = pathname+'/'+f
			if mode == 'path' :
				files.append(name)
			elif mode == 'name' :
				files.append(f)
			else :
				files.append((name,f))
		elif len(parts) == 1 :
			if f == pattern :
				name = pathname+'/'+f
				if mode == 'path' :
					files.append(name)
				elif mode == 'name' :
					files.append(f)
				else :
					files.append((name,f))
		else :
			prefix = parts[0] == '' or f.startswith(parts[0])
			suffix = parts[1] == '' or f.endswith(parts[1])
			if prefix and suffix :
				name = pathname+'/'+f
				if mode == 'path' :
					files.append(name)
				elif mode == 'name' :
					files.append(f)
				else :
					files.append((name,f))

	# RETURN LIST OF FULL PATHS AND FILENAMES THAT MATCH
	return files

if __name__ == '__main__' :

	print ('Testing vectorize()...')
	def nix (x,c0,c1) : return c0+c1*x
	print ('nix=',nix(*(1,2,3)))
	y = np.arange(10)
	a = 1
	b = 10
	print (vectorize (nix,y,*(a,b)))

	import parse
	print ('Testing strip_format...')
	fmt = 'COE{0:03d}_{1:1d}'
	s = fmt.format(2,3)
	print (s)
	stuff = sscanf(fmt,s)
	print (stuff,len(stuff))

#!/usr/bin/env python3

# pyfu/wavcal.py

import bisect
import logging
import numpy as np
import yaml

from astropy.io    import fits
from astropy.table import Table
from scipy         import signal, optimize
from matplotlib    import pyplot as plt

from numpy.polynomial.polynomial import polyfit,polyval
from numpy.polynomial.legendre   import legfit,legval
from numpy.polynomial.laguerre   import lagfit,lagval
from numpy.polynomial.hermite    import hermfit,hermval

from pyFU.utils    import centroid, cubic, read_tables, line, vectorize, write_tables, Gaussian1D
from pyFU.display  import show_with_menu

logging.basicConfig (level=logging.INFO,format='%(asctime)s * %(levelname)s * %(message)s')

MIN_WAVELENGTH = 300. # nm
MAX_WAVELENGTH = 1000. # nm

def pixel2wave (x, p, model='linear') :
	"""
	Converts a pixel position x into a wavelength w using one
	of several invertable models:

	'linear','quadratic','cubic'	w(x)=invert_polynomial(x,*p)= e.g. (x-p[0])/p[1]=(-p[0]/p[1])+(1/p[1])*x
	'exp'							w(x)=-ln[(x-p2)/p0]/p1=(p0/p1)+(-1)*ln[x-p2]
	'power'							w(x)=(x/p0)**(1/p1)

	The "inversion" is relevant because pixel(lambda) is the natural fitting
	routine (wavelengths are usually known), not lambda(pixel).
	"""
	if model in ['linear','quadratic','cubic'] :
		return invert_polynomial (x,p)
	elif model == 'exp' :
		return -np.log((x-p[2])/p[0])/p[1]
	elif model == 'power' :
		return (x/p[0])**(1/p[1])
	else :
		raise NotImplementedError ('model {0} not implemented'.format(model))

def wave2pixel (w, p, model='linear') :
	"""
	Converts a wavelength w into a pixel position x using one
	of several invertable models:

		'linear','quadratic','cubi'		x(w) = p0+p1*w+p2*w**2+p3*w**3
		'exp'							x(w) = p0*exp(-p1*w)+p2
		'power'							x(w) = p0*w**p1

	The "inversion" is relevant because pixel(lambda) is the natural fitting routine
	(wavelengths are known), not lambda(pixel).
	"""
	if model in ['linear','quadratic','cubic'] :
		x = w-w
		ww = 1.0
		for pp in p :
			x += pp*ww
			ww *= w
		return x
	elif model == 'exp' :
		return p[0]*np.exp(-p[1]*w)+p[2]
	elif model == 'power' :
		return p[0]*w**p[1]
	else :
		raise NotImplementedException ('model {0} not implemented'.format(model))

def invert_polynomial (x, p) :
	"""
	Inverts the polynomial

		x(w) = p0+p1*w+p2*w**2+p3*w**3

	to get w(x)
	"""
	np = len(p)
	# LINE
	if np == 2 :	# x = p0+p1*w
		return (x-p[0])/p[1]
	# QUADRATIC
	elif np == 3 :	# x = p0+p1*w+p2*w**2
		return invert_quadratic (x,p)
	# CUBIC
	elif np == 4 :
		return invert_cubic (x,p)
	# ERROR
	else :
		raise NotImplementedError ('polynomials with np={0} not supported'.format(np))

def invert_quadratic (x, p) :
	return vectorize (_invert_quadratic,x,p)

def _invert_quadratic (x, p) :
	c,b,a = p
	w1 = np.nan
	w2 = np.nan
	if a == 0. :
		w1 = -c/b
	else :
		eps = 1.
		if b < 0. : eps = -1.
		dis = b*b-4.*a*c
		if dis < 0. : return np.nan
		term = -b-eps*np.sqrt(dis)/a
		if term == 0. :
			w1 = 0.
			w2 = 0.5*(-b+eps*np.sqrt(dis))/a
		else :
			w1 = 0.5*(-b-eps*np.sqrt(dis))/a
			w2 = 2.*c/(-b-eps*np.sqrt(dis))
	if not np.isnan(w1) and w1 >= MIN_WAVELENGTH and w1 <= MAX_WAVELENGTH :
		return w1
	elif not np.isnan(w2) and w2 >= MIN_WAVELENGTH and w2 <= MAX_WAVELENGTH :
		return w2
	else :
		return np.nan

def invert_cubic (x, p) :
	return vectorize (_invert_cubic,x,p)

def _invert_cubic (x, p) :
	d,c,b,a = p
	w1 = np.nan
	w2 = np.nan
	w3 = np.nan
	third = 1./3.
	d -= x
	if a == 0. :
		return _invert_quadratic (x, b,c,d)
	w1 = np.nan
	w2 = np.nan
	w3 = np.nan
	pp = (3.*a*c-b*b)/(3.*a*a)
	qq = (2.*b*b*b-9.*a*b*c+27.*a*a*d)/(27.*a*a*a)
	delta = qq*qq/4.+pp*pp*pp/27.		# DISCRIMINANT

	if delta == 0. :	# MULTIPLE SOLUTIONS
		if qq == 0. :
			w1 = 0.
		elif q >= 0. :
			t1 = (0.5*qq)**third
			t2 = -2.*t1
			w1 = t1-third*b/a
			w2 = t2-third*b/a

	elif delta > 0. :	# ONE REAL SOLUTION
		au = -0.5*qq+np.sqrt(delta)
		u1 = np.abs(au)**third
		if au < 0. :
			u1 *= -1.
		av = -0.5*qq-np.sqrt(delta)
		v1 = np.abs(av)**third
		if av < 0. :
			v1 *= -1.
		t1 = u1+v1
		w1 = t1-third*b/a

	else :				# 3 REAL SOLUTIONS
		absp = np.abs(pp)
		arg0 = -0.5*qq*(3./absp)**1.5
		arg1 = third*np.pi/3.
		arg2 = arg1+2*np.pi/3.
		arg3 = arg1+4.*np.pi/3.
		f = 2.*np.sqrt(third*absp)

		t1 = f*np.cos(arg1)
		t2 = f*np.cos(arg2)
		t3 = f*np.cos(arg3)
		w1 = t1-third*b/a
		w2 = t2-third*b/a
		w3 = t3-third*b/a

	if not np.isnan(w1) and w1 >= MIN_WAVELENGTH and w1 <= MAX_WAVELENGTH :
		return w1
	elif not np.isnan(w2) and w2 >= MIN_WAVELENGTH and w2 <= MAX_WAVELENGTH :
		return w2
	elif not np.isnan(w3) and w3 >= MIN_WAVELENGTH and w3 <= MAX_WAVELENGTH :
		return w3
	else :
		return np.nan

def cc_calibrate (wav,flx, refwav,refflx, show=True, spacing=100, width=100, model='linear', flux_order=None, flux_func='laguerre') :
	"""
	Computes a wavelength calibration for a spectrum defined by arrays 'wav' and
	'flx'.  The wavelength table "wav" is assumed to be a rough wavelength scale
	good enough for setting the cross-correlation region, i.e. does not have to be
	very accurate.

	Returns the wavelength coefficients, the covariance matrix of those coefficients,
	the red.chi^2, the R.M.S., and the flux-calibration function of pixel-position only.

	If "flux_order" is not None, then the wavelength chunks are also used to perform a
	rough flux correction relative to the reference spectrum using a polynomial fit of
	that order.
	"""
	print ('width=',width,', spacing=',spacing)
	# ---- FOR EACH CHUNK OF DATA SPECTRUM

	pixels     = []	# MEAN DATA PIXEL OF EACH CHUNK
	waves      = []	# MEAN WAVELENGTH OF EACH CHUNK
	err_pixels = [] # PIXEL ERRORS DERIVED FROM C-C FUNCTION
	fcorr      = [] # FLUX CORRECTION FACTORS
	err_fcorr  = [] # FLUX CORRECTION FACTORS

	if flux_func is None :
		pass
	else :
		flxfnc = flux_func.lower()
		if flxfnc.startswith('poly') :
			fit_func = polyfit
			fit_val  = polyval
		elif flxfnc.startswith('lag') :
			fit_func = lagfit
			fit_val  = lagval
		elif flxfnc.startswith('leg') :
			fit_func = legfit
			fit_val  = legval
		elif flxfnc.startswith('her') :
			fit_func = hermfit
			fit_val  = hermvals
		else :
			fit_func = polyfit
			fit_val  = polyval
			logging.info ('using normal polynomials for flux calibration')

	nwav = len(wav)
	pix = np.arange(nwav)

	width += width%2	# WANT ODD NUMBER OF PIXELS IN CHUNK
	n1 = 0
	n2 = n1+width-1

	showing = show
	# FOR ALL WAVELENGTH CHUNKS...
	while n2 < nwav :
		w = wav[n1:n2+1]
		n = n2-n1+1
		x = np.arange(n)
		hanning = signal.hanning (n)
		pixavg = 0.5*(n1+n2)
		wavg = np.mean(w)
		disp = (wav[n2]-wav[n1])/(n-1)

		# GET CHUNK OF SPECTRUM
		s = flx[n1:n2+1]
		scoef,scov = optimize.curve_fit (line,w,s)	# FIT LINE TO DATA IN WINDOW
		sfit = line(w,scoef[0],scoef[1])
		snorm = np.nanmean(s)

		# GET CORRESPONDING CHUNK OF REFERENCE
		r = np.interp(w,refwav,refflx)
		rcoef,rcov = optimize.curve_fit (line,w,r)	# FIT TO DATA IN REFERENCE
		rfit = line(w,rcoef[0],rcoef[1])
		rnorm = np.nanmean(r)
		corr = rnorm/snorm

		# PLOT CHUNKS
		f1 = (s-sfit)/snorm
		f2 = (r-rfit)/rnorm
		fscal = np.max(np.abs(f1))
		if showing :
			fig = plt.figure ()
			plt.style.use ('ggplot')
			plt.title ('chunk {0}-{1} data'.format(n1,n2))
			plt.xlabel ('wavelength [nm]')
			plt.plot (w,f1*hanning,color='gray',label='data*window')
			plt.plot (w,f2,color='red', label='ref')
			plt.plot (w,f1,color='black',label='data')
			plt.legend ()
			plt.tight_layout ()
			stat = show_with_menu (fig,['RESULTS','ABORT'])
			if stat == 'RESULTS' :
				showing = False
			elif stat == 'ABORT' :
				return None,None,None,None,None

		# CROSS-CORRELATE CHUNKS
		ycorr = np.correlate (f1*hanning,f2,mode='same')
		yscal = np.max(np.abs(ycorr))
		xcorr = x-n//2
		mask = signal.tukey (len(ycorr))*np.abs(np.max(ycorr))
		tukey = mask/np.max(mask)
		if showing :
			fig = plt.figure ()
			plt.style.use ('ggplot')
			plt.title ('C-C of chunk {0}-{1}'.format(n1,n2))
			plt.xlabel ('shift in pixels')
			plt.plot (xcorr,ycorr*tukey,color='gray',label='C-C*window')
			plt.plot (xcorr,ycorr,color='black',label='C-C')

		ycorr *= tukey

		# FIND PEAK OF C-C FUNCTION
		peak,wid = centroid(xcorr,ycorr,9)
		errpeak = 1.0
		errfcorr = 0.1*corr
		try :
			# FIT GAUSSIAN TO C-C FUNCTION: ycorr ~ a+b*exp(-(xcorr-c)**2/d**2)
			p0 = [0.05*peak,np.max(ycorr),peak,wid/2.]
			p,cov = optimize.curve_fit (Gaussian1D,xcorr,ycorr,p0=p0)
			rms2 = np.nansum((ycorr-Gaussian1D (xcorr,*p))**2)/len(xcorr)
			peak = p[2]
			wid = p[3]
			errpeak = np.sqrt(cov[2][2]+0.01*wid**2)
			rrfcorr = corr*np.sqrt(cov[1][1]+rms2)/snorm
			if showing :
				plt.plot (xcorr,Gaussian1D(xcorr,*p),'-.',label='fit',color='blue')
		except :
			peak = np.nan

		if showing :
			plt.legend ()
			plt.tight_layout ()
			stat = show_with_menu (fig,['RESULTS','ABORT'])
			if stat == 'RESULTS' :
				showing = False
			elif stat == 'ABORT' :
				return None,None,None,None,None

		if np.isnan(peak) :
			logging.warning ('chunk {0:.2f} at ~{1:.2f} has bad peak'.format(pixavg,wavg))

		elif peak > -n/3 and peak < n/3 :	# USE MIDDLE OF C-C FUNCTION!
			logging.debug ('chunk {0:.2f} at ~{1:.2f} produced x_peak={2:.2f},dw_peak={3:.4f}'.format(pixavg,wavg,peak,peak*disp))

			# SAVE CALIBRATION POINT: AVG PIXEL WHERE WAVELENGTHS MATCH IS OFF BY THE PEAK SHIFT
			pixels.append(pixavg+peak)
			waves.append(wavg)
			err_pixels.append(errpeak)

			# SAVE FLUX CORRECTION FACTOR
			fcorr.append(corr)
			err_fcorr.append (errfcorr)
		else :
			logging.warning ('chunk {0} produced peak too near edge {1}'.format(pixavg,peak))


		# NEXT CHUNK
		n1 += spacing
		n2 += spacing
		if n2 >= nwav : n2=nwav-1
		if (n2-n1) < width//2 : n2=nwav

	# FIT FLUX CORRECTION FACTORS
	cflux = None
	if flux_func is not None and flux_order is not None :
		if flux_order < 1 :
			logging.error ('flux-correction order < 1: {0}'.format(flux_order))
		else :
			fcorr /= np.nanmedian(fcorr)
			cflux = fit_func (pixels,fcorr,flux_order,w=1./np.array(err_fcorr))
			if show :
				plt.style.use ('ggplot')
				plt.tight_layout ()
				plt.xlabel ('pixel')
				plt.ylabel ('flux correction')
				plt.plot (pix,fit_val(pix,cflux),'-',color='green')
				plt.errorbar (pixels,fcorr,yerr=err_fcorr,fmt='o',color='red')
				plt.ylim (bottom=0.)
				plt.show ()
			flx *= fit_val (pix,cflux)

	# RETURN RESULTING WAVELENGTH CALIBRATION
	wcoef,cov,rchi2,rms = wave_fit (pixels,err_pixels,waves,model=model,show=show)
	return wcoef,cov,rchi2,rms, lambda x: fit_val (x,cflux)

def wave_fit (x,xerr,w, model='linear', show=False) :
	"""
	Fits x(w) to the data using the given model.
	Returns coefficients,covariance_matrix,red.chi^2, and R.M.S.
	"""
	pix = np.array(x)
	wav = np.array(w)
	err = np.array(xerr)
	n = len(pix)

	# SET UP MODEL
	if model == 'linear' :
		w2p = lambda w,c0,c1 : wave2pixel (w,[c0,c1],model=model)
		title = 'pixel(w)=c0+c1*w'
	elif model == 'quadratic' :
		w2p = lambda w,c0,c1,c2 : wave2pixel (w,[c0,c1,c2],model=model)
		title = 'pixel(w)=c0+c1*w+c2*w^2'
	elif model == 'cubic' :
		w2p = lambda w,c0,c1,c2,c3 : wave2pixel (w,[c0,c1,c2,c3],model=model)
		title = 'pixel(w)=c0+c1*w+c2*w^2+c3*w^3'
	elif model == 'exp' :
		w2p = lambda w,c0,c1 : wave2pixel (w,[c0,c1],model=model)
		title = 'pixel(w)=p0*exp(-p1*w)+p2'
	elif model == 'power' :
		w2p = lambda w,c0,c1 : wave2pixel (w,[c0,c1],model=model)
		title = 'pixel(w)=c0*w^c1'

	# FIT
	coef,cov = optimize.curve_fit (w2p,wav,pix,sigma=err)

	# RE-FIT FOR ROBUSTNESS
	mask = np.ones(n,dtype=bool)
	for iter in range(3) :
		rms = 0.0
		for i in range(n) :
			if mask[i] :
				xfit = w2p(wav[i],*coef)
				d = pix[i]-xfit
				rms += d**2
		m = np.sum(mask)
		rms = np.sqrt(rms/m)
		logging.info ('iteration #{0}: R.M.S.: {1:.2f}'.format(iter,rms))
		for i in range(n) :
			xfit = w2p(wav[i],*coef)
			d = np.abs(pix[i]-xfit)/rms
			if d > 3 : mask[i] = False
			err[i] = np.sqrt(err[i]**2+(rms*d)**2)
		m = np.sum(mask)
		coef,cov = optimize.curve_fit (w2p,wav[mask],pix[mask],sigma=err[mask])
	if m != n :
		logging.info ('removed {0} data points with >3*R.M.S. deviation'.format(n-m))

	logging.info ('# calibration {0}'.format(title))
	fmt = '#\tcoef[{0}] = {1:10.4e} +/- {2:10.4e}'
	for i in range(len(coef)) :
		logging.info (fmt.format(i,coef[i],np.sqrt(cov[i][i])))
	logging.info ('# No.   Wavelength Pixel     Err(Pixel) Fit(Pixel)  Diff      Diff/Err')
	fmt = '# {0:3d}  {1:8.3f}   {2:8.3f} {3:7.3f}     {4:8.3f}   {5:8.3f}  {6:8.3f}'
	rchi2 = 0.0
	for i in range(n) :
		if mask[i] :
			xfit = w2p(wav[i],*coef)
			d = (pix[i]-xfit)/err[i]
			logging.info (fmt.format(i+1,wav[i],pix[i],err[i],xfit,d*err[i],d))
			rchi2 += d**2
	rms = np.sqrt(rchi2/m)
	rchi2 /= (m-len(coef))
	logging.info ('# red. chi^2 = {0:.3f}, R.M.S.={1:.3f}'.format(rchi2,rms))

	# DISPLAY
	if show :
		# PLOT DATA AND FIT
		plt.style.use ('ggplot')
		plt.tight_layout ()
		fig = plt.figure ()
		fig.subplots_adjust(right=0.8)

		plt.subplot (2,1,1)
		plt.xlabel ('wavelength [nm]')
		plt.ylabel ('pixel')
		plt.plot (wav,w2p(wav,*coef),'-',color='green')
		plt.errorbar (wav,pix,yerr=err,fmt='o',color='red')
		plt.errorbar (wav[mask],pix[mask],yerr=err[mask],fmt='o',color='black')
		plt.title (title)

		# PLOT RESIDUALS
		plt.subplot (2,1,2)
		plt.xlabel ('wavelength [nm]')
		plt.ylabel ('residual [pix]')
		dxfit = pix-w2p(wav,*coef)
		plt.plot ([wav[0],wav[-1]],[0.,0.],'-',color='green')
		plt.errorbar (wav,dxfit,yerr=err,fmt='o',color='red')
		plt.errorbar (wav[mask],dxfit[mask],yerr=err[mask],fmt='o',color='black')

		# SHOW CALIBRATION IN PLOT
		fmt = 'c{0}={1:10.4e}'
		dw = wav[-1]-wav[0]
		pmax = np.max(dxfit+err)
		dp = pmax-np.min(dxfit-err)
		y = pmax-0.1*dp
		for i in range(len(coef)) :
			plt.text (w[-1]+0.05*dw,y, fmt.format(i,coef[i]))
			y -= 0.1*dp
		plt.text (w[-1]+0.05*dw,y,'red. chi^2 : {0:.2f}'.format(rchi2))
		y -= 0.1*dp
		plt.text (w[-1]+0.05*dw,y,'R.M.S. : {0:.2f}'.format(rms))

		plt.show ()

	# RETURN RESULTS
	return coef,cov,rchi2,rms

def cc_calibrate_spectra (spectra,refwav,refflx,
			pixcol='pixel',wavcol='wavelength',flxcol='flux',
			approx=None, show=False, width=100, spacing=100,
			model='linear', flux_order=None, flux_func='laguerre',
			prompt=False) -> bool :
	"""
	Calibrates a list of spectra in astropy.table.Tables by cross-correlating each spectrum with a reference.
	"""
	# GLOBAL RESULTS
	idxs  = []
	disps = []
	chi2s = []
	rmss  = []
	showed = show

	# FOR EACH SPECTRUM...
	logging.info ('cc_calibrate_spectra: spacing={0}, width={1}'.format(spacing,width))
	for idx in range(len(spectra)) :
		logging.info ('---- spectrum #{0} ----'.format(idx))
		spectrum = spectra[idx]
		pix  = spectrum[pixcol]
		flux = spectrum[flxcol]

		# GET APPROXIMATE WAVELENGTH SCALE
		if wavcol in spectrum.colnames :
			wav = spectrum[wavcol]
		elif approx is not None and len(approx) > 1 :
			wav = 0.0
			for i in range(len(approx)) :
				wav += approx[i]*pix**i
		else :
			logging.error ('No approximate wavelength scale available for spectrum #{0}'.format(idx))
			return False

		# CALIBRATE
		pcoef,cov,rchi2,rms,flxcorr = cc_calibrate (wav,flux, refwav,refflx, show=show, width=width, spacing=spacing, model=model, flux_order=flux_order, flux_func=flux_func)
		if pcoef is None :
			return False
		w = pixel2wave (pix,pcoef, model=model)
		spectrum[wavcol] = w
		spectrum['flux_correction'] = flxcorr(pix)
		chi2s.append(rchi2)
		rmss.append(rms)

		# DISPLAY RESULTS
		if show :
			factor = np.nanmedian(flux)/np.nanmedian(refflx)
			fig = plt.figure ()
			plt.style.use ('ggplot')
			plt.tight_layout ()
			plt.xlabel ('wavelength [nm]')
			plt.ylabel ('flux')
			# plt.title ('spectrum #{0}'.format(idx))
			plt.plot (refwav,refflx*factor,'-',color='blue',label='ref')
			plt.plot (w,flux,'-',color='black',label='#{0}'.format(idx))
			plt.legend (fontsize='x-small')	# bbox_to_anchor=(1.02,1), loc='upper left', ncol=1, fontsize='x-small')
			if prompt :
				plt.show ()
				ans = input ('(aBORT,sILENT) :').lower()
				if ans.startswith('a') : return False
				elif ans.startswith('s') : show = False
			else :
				reslt = show_with_menu (fig,['no more plots','ABORT'])
				if reslt == 'no more plots' :
					show = False
				elif reslt == 'ABORT' :
					return False

		if showed : # SAVE RESULTS
			idxs.append(idx)
			dwdp = np.median(np.diff(w)/np.diff(pix))
			disps.append(dwdp)

	rchi2 = np.mean(np.array(chi2s))
	rms   = np.mean(np.array(rmss))
	logging.info ('Mean red.chi^2,R.M.S. of all spectra: {0:.3f},{1:.3f}'.format(rchi2,rms))

	if showed :
		plt.style.use ('ggplot')
		plt.xlabel ('Spectrum index')
		plt.ylabel ('Median Dispersion [nm/pixel]')
		d = np.array(disps)
		std = np.std(d)
		plt.ylim (bottom=np.min(d)-std,top=np.max(d)+std)
		plt.plot (idxs,disps,'o')
		plt.tight_layout ()
		plt.show ()

	return True

def transfer_wavelengths_by_index (refs,extracted, pixcol='pixel', wavcol='wavelength', flxcol='flux') -> bool :
	"""
	Transfers the wavelength calibration of a list of reference spectra to a set of
	similarly extracted but not wavelength calibrated spectra.
	"""
	n = len(refs)
	if n != len(extracted) :
		logging.critical ('transfer_calibration: different number of spectra!')
		return False

	"""
	refidx = {}
	for i in range(n) :
		spectrum = refs[i]
		hdr = spectrum.meta
		key = keywds['index'][0]
		if key not in hdr :
			logging.error ('no index metadata for reference spectrum #{0}'.format(i))
		else :
			refidx[hdr[key]] = i
	"""

	for i in range(n) :
		ref      = refs[i]
		spectrum = extracted[i]
		refcols = ref.colnames
		cols = spectrum.colnames
		hdr = spectrum.meta
		if wavcol not in refcols :
			logging.error ('transfer_calibration: wavelength column names not present in reference #{0}!'.format(i))
			return False
		elif len(ref) != len(spectrum) :
			logging.error ('spectrum and target #{0} of different lengths ({1}!={2})!'.format(i,len(ref),len(spectrum)))
			return False
		else :
			spectrum[wavcol] = ref[wavcol]+0.
			hdr['comment'] = 'transfered wavelength calibration from source table'
			if 'flux_correction' in ref.colnames :
				if flxcol not in cols :
					logging.error ('transfer_calibration: flux column name not present in target spectrum #{0}!'.format(i))
					return False
				spectrum['flux_correction'] = ref['flux_correction']+0.
				spectrum[flxcol] *= spectrum['flux_correction']
				hdr['comment'] = 'transfered flux correction from source table'
	return True

if __name__ == '__main__' :

	import sys
	from pyFU.utils import parse_arguments

	logging.info ('*************************** wavcal ******************************')

    # ---- GET DEFAULTS AND PARSE COMMAND LINE
	README = """
Python script that performs a wavelength-calibration using localized
cross-correlation with a standard spectrum of similar resolution.
Requires that the spectrum to be calibrated has roughly the same
wavelength calibration so that the regions to be cross-correlated are
reasonably well-defined.
	"""
	arguments = {
		'approx':{'path':'wavcal:','default':None,'dshow':None,'flg':'-a','type':str,'help':'rough wavelength calibration w0,d0 where wav ~ w0+d0*pix'},
		'fcol':{'path':'wavcal:','default':'flux','dshow':'flux','flg':'-f','type':str,'help':'name of flux table column in reference'},
		'flxcol':{'path':'wavcal:','default':'flux','dshow':'flux','flg':'-F','type':str, 'help':'name of output flux table column)'},
		'flux_function':{'path':'wavcal:','default':None,'dshow':None,'flg':'-X','type':str,'help':'function for flux-calibration; polynomial|legendre|laguerre|hermite (default laguerre)'},
		'flux_order':{'path':'wavcal:','default':None,'dshow':None,'flg':'-C','type':str,'help':'polynomial order for C-C flux-correction'},
		'source':{'path':'wavcal:','default':None,'dshow':None,'flg':'-S','type':str,'help':'pathname of wavelength and flux calibration spectra'},
		'infile':{'path':'wavcal:','default':None,'dshow':None,'flg':'-i','type':str,'help':'pathname of input FITS or ascii table'},
		'in_format':{'path':'wavcal:','default':None,'dshow':None,'flg':'-I','type':str,'help':'optional format for finding spectrum in the pathname directory'},
		'outfile':{'path':'wavcal:','default':None,'dshow':None,'flg':'-o','type':str, 'help':'output FITS table'},
		'out_format':{'path':'wavcal:','default':None,'dshow':None,'flg':'-O','type':str,'help':'optional format for writing calibrated spectra to the pathname directory'},
		'model':{'path':'wavcal:','default':None,'dshow':'linear','flg':'-m','type':str,'help':'model (linear|quadratic|cubic|exp|power; default linear)'},
		'pause':{'path':None,'default':False,'dshow':False,'flg':'-P','type':bool,'help':'pause/prompt after every spectral calibration'},
		'pixcol':{'path':'wavcal:','default':'pixel','dshow':'pixel','flg':'-x','type':str,'help':'name of pixel table column in target (default pixel)'},
		'plot':{'path':None,'default':False,'dshow':False,'flg':'-p','type':bool,'help':'plot result'},
		'reference':{'path':'wavcal:','default':None,'dshow':None,'flg':'-r','type':str,'help':'pathname of FITS reference spectrum'},
		'spacing':{'path':'wavcal:','default':None,'dshow':None,'flg':'-s','type':int,'help':'spacing of cross-correlation windows [pix]'},
		'wcol':{'path':'wavcal:','default':'wavelength','dshow':'wavelength','flg':'-w','type':str,'help':'name of wavelength table column in reference'},
		'wavcol':{'path':'wavcal:','default':'wavelength','dshow':'wavelength','flg':'-W','type':str,'help':'name of output wavelength table column'},
		'window_cc':{'path':'wavcal:','default':None,'dshow':None,'flg':'-N','type':int,'help':'size of cross-correlation windows [pix]'},
		'window_centroid':{'path':'wavcal:','default':11,'dshow':None,'flg':'-c','type':int,'help':'size of cross-correlation centroid window [pix]'},
		'yaml':{'path':None,'default':None,'dshow':None,'flg':'-y','type':str,'help':'global YAML configuration file for parameters'}
		}
	args,cfg = parse_arguments (arguments)
	info = cfg['wavcal']

	if 'approx' in info and info['approx'] is not None and isinstance(info['approx'],str) :
		a = info['approx'].split(',')
		info['approx'] = np.array([float(a[0]),float(a[1])])

	# ---- NEED EITHER REFRENCE OR SOURCE
	if ('source' not in info or info['source'] is None) and ('reference' not in info or info['reference'] is None) :
		logging.critical ('No reference or source spectra given')
		sys.exit(1)

	# ---- GET ALL THE DATA
	if 'infile' not in info or info['infile'] is None :
		logging.critical ('No input spectrum given')
		sys.exit(1)
	if 'in_format' not in info :
		info['in_format'] = None
	spectra,header = read_tables (pathname=info['infile'],fmt=info['in_format'])
	if len(spectra) == 0 :
		logging.critical ('No spectra read from {0}'.format(info['infile']))
		sys.exit(1)

	# ---- GET REFERENCE
	if 'reference'in info and info['reference'] is not None :
		print ('reference:',info['reference'])
		reftab,hdr = read_tables (pathname=info['reference'])
		if len(reftab) != 1 :
			logging.critical ('Reference file does not contain a single spectrum!')
			sys.exit(1)
		refwav = reftab[0][args.wcol]
		refflx = reftab[0][args.fcol]

		# ---- CALIBRATE EACH SPECTRUM
		if not cc_calibrate_spectra (spectra,refwav,refflx,
				 	pixcol=info['pixcol'],wavcol=info['wavcol'],flxcol=info['flxcol'],
					approx=info['approx'],show=args.plot,
					width=info['window_cc'], spacing=info['spacing'],
					model=info['model'], flux_order=info['flux_order'],
					flux_func=info['flux_function'],
					prompt=info['pause']) :
			logging.error ('ABORTED spectral calibration!')
			sys.exit(1)

	# ---- GET SOURCE
	elif 'source' in info and info['source'] is not None :
		print ('source:',info['source'])
		reftab,hdr = read_tables (pathname=info['source'])

		# ---- TRANSFER WAVELENGTH AND FLUX CALIBRATIONS
		if not transfer_wavelengths_by_index (reftab,spectra, wavcol=info['wavcol'], flxcol=info['flxcol']) :
			logging.error ('ABORTED spectral calibration!')
			sys.exit(1)

	# ---- SAVE RESULTS
	if 'outfile' in info and info['outfile'] is not None :
		logging.info ('Saving to FITS table file {0}...'.format(info['outfile']))
		if 'out_format' not in info :
			info['out_format'] = None
		write_tables (spectra,header=header,pathname=info['outfile'],fmt=info['out_format'])


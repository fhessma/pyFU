#!/usr/bin/env python3

# pyfu/image.py 

"""
Method for turning extracted spectra into IFU images.
"""

import numpy as np
import sys
import logging

from astropy.io    import fits
from astropy.table import Table, Column
from astropy.wcs   import WCS

from matplotlib          import pyplot as plt, cm as colourmaps
from matplotlib.colors   import Normalize
from matplotlib.colorbar import ColorbarBase

from pyFU.defaults import defaultFormats, defaultKeywords
from pyFU.ifu      import Fibre, get_fibres
from pyFU.utils    import hist_integral, header2config, merge_dictionaries

logging.basicConfig (level=logging.INFO,format='%(asctime)s * %(levelname)s * %(message)s')

def spectra2fits (spectra, config=None, keywords=defaultKeywords, formats=defaultFormats, orig_header=None) :
	"""
	Produces a 2- or 3-dimensional IFU image from a series of extracted IFU spectra.
	The spectral tables are expected to have metadata corresponding to a particular fibre.
	"""
	# GET THE DATA-TO-IMAGE CONFIGURATION

	naxis = 2
	key = 'naxis'
	if info is not None and key in info :
		naxis = info[key]
	logging.info ('naxis : {0}'.format(naxis))

	naxis1 = 1000
	key = 'naxis1'
	if info is not None and key in info :
		naxis1 = info[key]
	logging.info ('naxis1 : {0}'.format(naxis1))

	naxis2 = 1000
	key = 'naxis2'
	if info is not None and key in info :
		naxis2 = info[key]
	logging.info ('naxis2 : {0}'.format(naxis2))

	naxis3 = None
	key = 'naxis3'
	if info is not None and key in info :
		naxis3 = info[key]
	logging.info ('naxis3 : {0}'.format(naxis3))

	waves = None
	key = 'waves'
	if config is not None and key in config :
		waves = config[key]
	logging.info ('waves: {0}'.format(str(waves)))

	# GET THE FIBRES FOR ALL SPECTRA AND THE TOTAL SIZE LIMITS
	xmax = -np.inf
	xmin =  np.inf
	ymax = -np.inf
	ymin =  np.inf
	fibres = []
	for i in range(len(spectra)) :
		spectrum = spectra[i]
		meta = spectrum.meta
		fibre = Fibre(meta)		# ASSUME ALL INFO IS IN THE METADATA
		fibres.append(fibre)

		x,y = fibre.pos
		r = fibre.inner_diameter/2.
		if x-r < xmin : xmin = x-r
		if x+r > xmax : xmax = x+r
		if y-r < ymin : ymin = y-r
		if y+r > ymax : ymax = y+r

	# SET THE PLATESCALE
	xsize = xmax-xmin
	ysize = ymax-ymin
	if xsize/naxis1 < ysize/naxis2 :
		platescale = xsize/naxis1
	else :
		platescale = ysize/naxis2

	# CREATE A WCS
	w = WCS(naxis=naxis)

	# CREATE NEW IMAGE HDU
	if naxis == 3 :
		data = np.zeros((naxis3,naxis2,naxis1))
		if waves is None :
			wav = spectra[0]['wavelength']
			wave1 = wav[0]
			wave2 = wav[-1]
		else :
			wave1 = waves[0]
			wave2 = waves[1]
		dwave = (wave2-wave1)/(naxis3-1)
		w.wcs.crpix = [naxis1//2,naxis2//2,1.]
		w.wcs.cdelt = [platescale,platescale,dwave]
		w.wcs.crval = [0.,0.,wave1]
	else :
		data = np.zeros((naxis2,naxis1))
		w.wcs.crpix = [naxis1//2,naxis2//2]
		w.wcs.cdelt = [platescale,platescale]
		w.wcs.crval = [0.,0.]

	# FILL IN EACH FIBRE
	for i in range(len(spectra)) :
		spectrum = spectra[i]
		fibre = fibres[i]

		# GET WAVELENGTHS & INTEGRATED FLUXES
		wavelength = spectrum['wavelength']
		flux = spectrum['flux']
		flx = np.sum(flux)	# hist_integral (wavelength,flux,None,None,wave1,wave2)

		x,y = fibre.pos
		r = fibre.inner_diameter/2.
		if x is not None and y is not None and r > 0. :
			if naxis == 2 :
				pix = w.wcs_world2pix (np.array([[x,y]]),0)
				ic = int(pix[0][0])
				jc = int(pix[0][1])
				mask = in_circle (ic,jc,r*platescale,naxis1,naxis2)
				if np.size(mask) == 0 :
					logging.info ('fibre #{0} not added to IFU image'.format(idx))
				else :
					logging.info ('adding fibre #{0} to IFU image ...'.format(idx))
					for i,j in mask :
						data[j][i] = flx

	# PREPARE FITS HDU
	hdu = fits.PrimaryHDU (data)	# =data,header=orig_hdr)
	hdr = hdu.header

	# ADD SPECIAL KEYWORDS AND RETURN
	if waves is not None :
		if 'wave1' in keywords :
			hdr[keywords['wave1']] = waves[0]
		if 'wave2' in keywords :
			hdr[keywords['wave2']] = waves[1]
	return hdu

def in_circle (i,j,r,naxis1,naxis2) :
	""" List of positions centered at i,j within r pixels of center """
	c = []
	r2 = r*r
	for ii in range(i-r,i+r+1,1) :
		for jj in range(j-r,j+r+1,1) :
			rr = (ii-i)**2+(jj-j)**2
			if rr < r2 :
				if ii >= 0 and jj >= 0 and ii < naxis1 and jj < naxis2 :
					c.append((ii,jj)) 
	return c

def spectra2image (spectra, title=None, pixels=None, waves=None, outfile=None, show=False, logscale=False, cmap='viridis', fudge=1.35) :
	"""
	Converts a list of extracted IFU spectra into an IFU focal plane image.
	"""
	nspectra = len(spectra)

	# GET RANGE OF VALUES
	fluxes = np.zeros(nspectra)
	for i in range(nspectra) :
		spectrum = spectra[i]
		f = spectrum['flux']
		if waves is not None :
			w = spectrum['wavelength']
			if waves[0] is None :
				wave1 = w[0]
			else :
				wave1 = waves[0]
			logging.debug ('minimum wavelength: {0:.3f}'.format(wave1))
			if waves[1] is None :
				wave2 = w[-1]
			else :
				wave2 = waves[1]
			logging.debug ('maximum wavelength: {0:.3f}'.format(wave2))
			val = np.nanmean(f[np.where ((w>=wave1)*(w<=wave2))])
		else :
			if 'pixel' in spectrum :
				pix = spectrum['pixel']
			else :
				pix = np.arange(len(f))
			if pixels is None or pixels[0] is None :
				pixel1 = pix[0]
			else :
				pixel1 = 0
			logging.debug ('minimum pixel: {0:.3f}'.format(pixel1))
			if pixels is None or pixels[1] is None :
				pixel2 = pix[-1]
			else :
				pixel2 = len(f)-1
			logging.debug ('maximum pixel: {0:.3f}'.format(pixel2))
			val = np.nanmean(f[np.where ((pix>=pixel1)*(pix<=pixel2))])
		fluxes[i] = val
	fluxes = np.array(fluxes)
	fmin = np.nanmin(fluxes)
	fmax = np.nanmax(fluxes)
	logging.info ('fmin={0:.3e}, fmax={1:.3e}'.format(fmin,fmax))
	if logscale :
		if fmax <= 0. :
			fmax += fmin
			logging.info ('added {0:.3e} to grayscaling values'.format(fmin))
			fmin = 0.
		if fmin <= 0. :
			fmin = 0.001*np.abs(fmax-fmin)
			logging.info ('set grayscaling basement to {0:.3e}'.format(fmin))

	# GET matplotlib COLORMAP
	cmap = colourmaps.get_cmap (cmap,128)

	# CREATE PLOT
	plt.style.use ('dark_background')
	fig,ax = plt.subplots ()
	xlim = [0.,0.]
	ylim = [0.,0.]
	for i in range(nspectra) :
		spectrum = spectra[i]
		f = fluxes[i]
		if logscale :
			gray = (np.log(f)-np.log(fmin))/(np.log(fmax)-np.log(fmin))
		else :
			gray = (f-fmin)/(fmax-fmin)
		fibre = Fibre (header=spectrum.meta)
		pos = fibre.pos
		din = fibre.inner_diameter
		if pos is not None and din is not None :
			if xlim[0] > pos[0]-din : xlim[0] = pos[0]-din
			if xlim[1] < pos[0]+din : xlim[1] = pos[0]+din
			if ylim[0] > pos[1]-din : ylim[0] = pos[1]-din
			if ylim[1] < pos[1]+din : ylim[1] = pos[1]+din
			circle = plt.Circle(pos,din/2,color=cmap(gray),edgecolor='white')
			ax.add_artist (circle)
			plt.text (pos[0],pos[1],fibre.label,horizontalalignment='center',verticalalignment='center',fontsize=8)
	plt.xlim (xlim)
	plt.ylim (ylim)
	ratio = fudge*(ylim[1]-ylim[0])/(xlim[1]-xlim[0])
	logging.info ('figure ratio: {0:.2f}'.format(ratio))
	if ratio < 1. :
		fig.set_figwidth(10.)
		fig.set_figheight (10.*ratio)
	else :
		fig.set_figwidth(10./ratio)
		fig.set_figheight(10.)
	plt.xlabel ('focal-plane x-position')
	plt.ylabel ('focal-plane y-position')
	if title is not None :
		plt.title (title)

	# AAAAAAAAAAAACCCCCCCCCCCCKKKKKKKKKKKKKKK!!!!!!!!!!!!!!
	# cbax = fig.add_axes([1.05, 0.05, 0.05, 0.8], clip_on=True)
	# ColorbarBase (cbax, norm=Normalize(vmin=fmin,vmax=fmax), cmap=cmap)
	plt.tight_layout()
	if outfile is not None :
		plt.savefig (outfile)
	if show :
		plt.show ()

if __name__ == '__main__' :
	import matplotlib.pyplot as plt
	import sys
	import yaml

	from pyFU.utils        import parse_arguments, read_tables

	logging.info ('\n*************************** image ******************************')

	# ---- GET DEFAULT CONFIGURATION AND COMMAND LINE PARAMETERS
	README = """
Script for converting a set of extracted spectra into a 2-dimensional IFU image.
	"""
	arguments = {
		'cmap':{'path':'image:','default':None,'dshow':'viridis','flg':'-c','type':str,'help':'matplotlib colour map name'},
		'fudge':{'path':'image:','default':1.35,'dshow':1.35,'flg':'-F','type':float,'help':'fudge factor for setting the image window size'},
		'infile':{'path':'image:','default':None,'dshow':None,'flg':'-i','type':str,'help':'path of FITS image file or image directory'},
		'logscale':{'path':'image:','default':False,'dshow':False,'flg':'-L','type':bool,'help':'show IFU image using logarithmic intensity scaling'},
		'outfile':{'path':'image:','default':None,'dshow':None,'flg':'-o','type':str,'help':'path of output image file (fits,png,jpg,...)'},
		'pixels':{'path':'image:','default':None,'dshow':None,'flg':'-x','type':list,'help':'integration pixels of output image'},
		'show':{'path':None,'default':False,'dshow':False,'flg':'-p','type':bool,'help':'display resulting image'},
		'waves':{'path':'image:','default':None,'dshow':None,'flg':'-w','type':list,'help':'integration wavelengths of output image'},
		'yaml':{'path':None,'default':None,'dshow':None,'flg':'-y','type':str,'help':'name of pyFU configuration file'}
		}
	args,cfg = parse_arguments (arguments)
	info = cfg['image']

	# ---- GET THE SPECTRA
	if 'infile' not in info :
		logging.critical ('no input file(s) given!')
		sys.exit(1)
	logging.info ('Reading {0} ...'.format(info['infile']))
	spectra,header = read_tables (pathname=info['infile'])
	nspectra = len(spectra)

	if 'outfile' in info and info['outfile'] is not None :
		logging.info ('Writing image to {0} ...'.format(info['outfile']))

	# ---- CONVERT SPECTRA TO A FITS HDU IMAGE
	if 'outfile' in info and info['outfile'].endswith('.fits') :
		raise NotImplementedError ('FITS output not yet supported!')

	# ---- OR CONVERT SPECTRA TO A matplotlib GRAPH AND HENCE TO A PIXEL IMAGE
	else :
		if 'waves' in info and info['waves'] is not None :
			w = info['waves']
			title = '{0} from {1:.2f} to {2:.2f} nm'.format(info['infile'],w[0],w[1])
			logging.info ('Producing wavelength-bounded focal-plane image {0}'.format(info['outfile']))
			if info['logscale'] :
				logging.info ('... with logarithmic intensity scale ...')
			spectra2image (spectra, title=title, waves=w,
							outfile=info['outfile'], show=args.show, logscale=info['logscale'], fudge=info['fudge'])
		elif 'pixels' in info and info['pixels'] is not None :
			p = info['pixels']
			title = '{0} from {1:.2f} to {2:.2f} pix'.format(info['infile'],p[0],p[1])
			logging.info ('Producing wavelength-bounded focal-plane image {0}'.format(info['outfile']))
			logging.info ('Producing pixel-bounded focal-plane image ...')
			spectra2image (spectra, title=info['infile'],pixels=info['pixels'],
							outfile=info['outfile'], show=args.show, logscale=info['logscale'], fudge=info['fudge'])
		else :
			logging.info ('Producing integrated focal-plane image {0}'.format(info['outfile']))
			spectra2image (spectra, title=info['infile'],
							outfile=info['outfile'], show=args.show, logscale=info['logscale'], fudge=info['fudge'])

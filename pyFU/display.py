#!/usr/bin/env python3

# display.py

import logging
import numpy as np
import matplotlib.pyplot as plt

from utils import get_image_limits
from astropy.io import fits

logging.basicConfig (level=logging.INFO,format='%(asctime)s * %(levelname)s * %(message)s')

def show_hdu (hdu, vmin=None, vmax=None, aspect=None, colourbar=False, flip=True, kappa=3.0) :
	"""
	Display an image from a FITS HDU.  If "kappa" is given, then only a region above and
	below (+/-kappa*stddev) the median value is displayed.
	"""
	hdr = hdu.header
	data = hdu.data
	xmin,xmax,ymin,ymax,zmin,zmax = get_image_limits (hdu,mode='outside')
	zmed,zsig = np.median(data),np.std(data)
	if vmax is not None :
		zmax = vmax
	elif kappa is not None :
		zmax = zmed+kappa*zsig
	if vmin is not None :
		zmin = vmin
	elif kappa is not None :
		zmin = zmed-kappa*zsig
	plt.clf()
	if flip :
		origin = 'lower'
	else :
		origin = 'upper'
	im = plt.imshow (data, interpolation='none', aspect=aspect, origin=origin,
				extent=(xmin,xmax,ymin,ymax), vmax=zmax, vmin=zmin)
	if colourbar :
		plt.colorbar(im)
	return im

def plot_tables (tables, idx=None, mode='individual', xkey='wavelength', ykey='flux', errkey=None,
				xlabel=None, ylabel=None, title=None, flxmin=0, flxmax=None, *args, **kwargs) :
	# plt.clf()
	n = len(tables)
	if idx is None :
		indices = range(n)
	else :
		if idx < n :
			indices = [idx]
			mode = 'individual'
		else :
			logging.error ('spectrum index={0} > number of spectra ({1})'.format(idx,n))
			return

	if mode != 'individual' :
		fig = plt.figure ()
		plt.style.use ('ggplot')
		plt.tight_layout ()

	# FOR ALL TABLES
	for i in indices :
		table = tables[i]
		if mode == 'individual' :
			fig = plt.figure ()
			plt.style.use ('ggplot')
			plt.tight_layout ()

		# PLOT SPECTRUM
		try :
			if errkey is None :
				plt.plot (table[xkey],table[ykey],'-', *args, **kwargs)
			else :
				plt.errorbar (table[xkey],yerr=table[errkey], *args, **kwargs)
		except KeyError as e :
			logging.error ('cannot access table column(s):'+str(e))
			logging.error ('available columns:'+str(table.colnames))
			return

		if mode == 'individual' :
			if flxmin is not None :
				plt.ylim (bottom=flxmin)
			if flxmax is not None :
				plt.ylim (top=flxmax)
			plt.xlabel (xkey)
			plt.ylabel (ykey)
			if xlabel is not None : plt.xlabel (xlabel)
			if ylabel is not None : plt.ylabel (ylabel)
			if title is None : 
				plt.title  ('{0}, #{1}/{2}'.format(title, i+1,n))
			else :
				plt.title  ('#{0}/{1}'.format(i+1,n))
			if show_with_menu (fig,['abort']) == 'abort' :
				return

	if mode is not None and mode != 'individual' :
		if flxmin is not None :
			plt.ylim (bottom=flxmin)
		if flxmax is not None :
			plt.ylim (top=flxmax)
		plt.xlabel (xkey)
		plt.ylabel (ykey)
		if xlabel is not None : plt.xlabel (xlabel)
		if ylabel is not None : plt.ylabel (ylabel)
		if title  is not None : plt.title  (title)
		plt.show ()

def cursor_input (data, show_method=show_hdu, *args, **kwargs) :
	"""
	Experimental method to display the data from a FITS HDU and then permit the user to measure
	points using the cursor.  Default display is of an image.
	"""
	show_method (data, *args, **kwargs)
	plt.setp(plt.gca(), autoscale_on=False)

	pts = []
	pt = plt.ginput(1, timeout=-1, show_clicks=True, mouse_add=1, mouse_pop=3, mouse_stop=2)
	print (pt,len(pt))
	while len(pt) == 1 :
		pts.append(pt)
		pt = plt.ginput(1, timeout=-1, show_clicks=True, mouse_add=1, mouse_pop=3, mouse_stop=2)
		print (pt,len(pt))
	return pts

def show_hex (hdus, xkey='IFU-XPOS', ykey='IFU-YPOS', wavelength=None) :
	"""
	Displays a set of IFU spectra contained in a FITS HDU table list as an image.
	The positions of the fibres are read from the headers.
	If no wavelength is given, the flux integrated over all wavelengths is used.
	"""
	pass


"""
Taken directly from the Matplotlib Widgets Menu example
https://matplotlib.org/gallery/widgets/menu.html#sphx-glr-gallery-widgets-menu-py
"""

import matplotlib.colors as colors
import matplotlib.patches as patches
import matplotlib.mathtext as mathtext
import matplotlib.artist as artist
import matplotlib.image as image

class ItemProperties (object):
	def __init__(self, fontsize=14, labelcolor='black', bgcolor='gray',
				 alpha=1.0):
		self.fontsize = fontsize
		self.labelcolor = labelcolor
		self.bgcolor = bgcolor
		self.alpha = alpha
		self.labelcolor_rgb = colors.to_rgba(labelcolor)[:3]
		self.bgcolor_rgb = colors.to_rgba(bgcolor)[:3]

class SimpleMenuItem (artist.Artist):
	parser = mathtext.MathTextParser("Bitmap")
	padx = 5
	pady = 5
	def __init__(self, fig, labelstr, props=None, hoverprops=None, on_select=None):
		artist.Artist.__init__(self)
		self.set_figure(fig)
		self.labelstr = labelstr
		if props is None:
			props = ItemProperties()
		if hoverprops is None:
			hoverprops = ItemProperties()
		self.props = props
		self.hoverprops = hoverprops
		self.on_select = on_select
		x, self.depth = self.parser.to_mask(
			labelstr, fontsize=props.fontsize, dpi=fig.dpi)
		if props.fontsize != hoverprops.fontsize:
			raise NotImplementedError(
				'support for different font sizes not implemented')
		self.labelwidth = x.shape[1]
		self.labelheight = x.shape[0]
		self.labelArray = np.zeros((x.shape[0], x.shape[1], 4))
		self.labelArray[:, :, -1] = x/255.
		self.label = image.FigureImage(fig, origin='upper')
		self.label.set_array(self.labelArray)
		self.rect = patches.Rectangle((0, 0), 1, 1)
		self.set_hover_props(False)
		fig.canvas.mpl_connect('button_release_event', self.check_select)

	def check_select(self, event):
		over, junk = self.rect.contains(event)
		if not over:
			return
		if self.on_select is not None:
			self.on_select(self)

	def set_extent(self, x, y, w, h):
		# print(x, y, w, h)
		self.rect.set_x(x)
		self.rect.set_y(y)
		self.rect.set_width(w)
		self.rect.set_height(h)
		self.label.ox = x + self.padx
		self.label.oy = y - self.depth + self.pady/2.
		self.hover = False

	def draw(self, renderer):
		self.rect.draw(renderer)
		self.label.draw(renderer)

	def set_hover_props(self, b):
		if b:
			props = self.hoverprops
		else:
			props = self.props
		r, g, b = props.labelcolor_rgb
		self.labelArray[:, :, 0] = r
		self.labelArray[:, :, 1] = g
		self.labelArray[:, :, 2] = b
		self.label.set_array(self.labelArray)
		self.rect.set(facecolor=props.bgcolor, alpha=props.alpha)

	def set_hover(self, event):
		'''check the hover status of event and return true if status is changed'''
		b, junk = self.rect.contains(event)
		changed = (b != self.hover)
		if changed:
			self.set_hover_props(b)
		self.hover = b
		return changed

class SimpleMenu(object):
	def __init__(self, fig, menuitems):
		self.figure = fig
		fig.suppressComposite = True
		self.menuitems = menuitems
		self.numitems = len(menuitems)
		maxw = max(item.labelwidth for item in menuitems)
		maxh = max(item.labelheight for item in menuitems)
		totalh = self.numitems*maxh + (self.numitems + 1)*2*SimpleMenuItem.pady
		x0 = 10
		y0 = 70
		width = maxw + 2*SimpleMenuItem.padx
		height = maxh + SimpleMenuItem.pady
		for item in menuitems:
			left = x0
			bottom = y0 - maxh - SimpleMenuItem.pady
			item.set_extent(left, bottom, width, height)
			fig.artists.append(item)
			y0 -= maxh + SimpleMenuItem.pady
		fig.canvas.mpl_connect('motion_notify_event', self.on_move)

	def on_move(self, event):
		draw = False
		for item in self.menuitems:
			draw = item.set_hover(event)
			if draw:
				self.figure.canvas.draw()
				break

show_result = None

def show_menu_reaction (item):
	global show_result
	print('you selected "%s"' % item.labelstr)
	show_result = item.labelstr

def show_with_menu (fig, items, on_select=show_menu_reaction, left=0.2, bottom=0.2, top=0.9, right=0.9) :
	"""
	To test:
		fig = plt.figure ()
		plt.plot (range(10),range(10),'o')
		r = show_with_menu (fig, ['do this','do that'])
		print (r)
	"""
	global show_result
	fig.subplots_adjust(left=left,bottom=bottom,top=top,right=right)
	props = ItemProperties(labelcolor='black', bgcolor='gray', fontsize=12, alpha=0.2)
	hoverprops = ItemProperties(labelcolor='white', bgcolor='blue', fontsize=12, alpha=0.2)
	menuitems = []
	for label in items :
		item = SimpleMenuItem(fig, label, props=props, hoverprops=hoverprops, on_select=on_select)
		menuitems.append(item)
	if len(menuitems) > 0 :
		menu = SimpleMenu(fig, menuitems)
	plt.show ()
	plt.close ()
	return show_result

if __name__ == '__main__' :

	import argparse
	import sys
	from pyFU.utils import read_tables
	from astropy.io import fits
	parser = argparse.ArgumentParser ()
	parser.add_argument ('--colourbar','-c',default=True,action='store_true',help='add colourbar')
	parser.add_argument ('--index','-I',default=None,type=int,help='HDU index of spectrum in table')
	parser.add_argument ('--image','-i',default=None,help='path of input FITS image')
	parser.add_argument ('--xcol','-x',default='wavelength',help='name of x-column in table')
	parser.add_argument ('--ycol','-y',default='flux',help='name of y-column in table')
	parser.add_argument ('--table','-t',default=None,help='path of input binary FITS table')
	parser.add_argument ('--zmax','-Z',default=None,help='upper displayed value')
	parser.add_argument ('--zmin','-z',default=None,help='lower displayed value')
	args = parser.parse_args ()

	if args.table is not None :
		spectra,header = read_tables (args.table)
		print ('Spectral plot from table of',len(spectra),'spectra ...')
		plot_tables (spectra,idx=args.index,mode='individual',xkey=args.xcol,ykey=args.ycol,title=args.table)
		if args.index is None :
			plt.title (args.table)
			plt.show()

	elif args.image is not None :
		print ('Full-window show_hdu version with aspect=\'auto\':')
		hdus = fits.open (args.image)
		show_hdu (hdus[0],aspect='auto',colourbar=args.colourbar,vmin=args.zmin,vmax=args.zmax)
		plt.title (args.image)
		plt.show()

	else :
		print ('No input data!')
		sys.exit(1)

	sys.exit(0)
	"""
	print ('Normal version with cursor input:')
	pts = cursor_input (hdus[0])
	print (pts)
	"""

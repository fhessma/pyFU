# pyFU/defaults.py

# ---- FORMATS

defaultFormats = {
	'ampl_format'           : 'TR-A{0:03d}{1:1d}',
	'trace_format'          : 'TR-T{0:03d}{1:1d}',
	'sigma_format'          : 'IF-SG{0:03d}',
	'inner_diameter_format' : 'IF-DI{0:03d}',
	'outer_diameter_format' : 'IF-DO{0:03d}',
	'index_format'          : 'IF-ID{0:03d}',
	'label_format'          : 'IF-LA{0:03d}',
	'spacing_format'        : 'IF-SP{0:03d}',
	'xpos_format'           : 'IF-XP{0:03d}',
	'ypos_format'           : 'IF-YP{0:03d}'
	}

# ---- KEYWORDS

defaultKeywords = {
	'ampl_coef'             : (None,'ampl. coefficient a(index,ord)'),
	'ampl_format'           : ('TR-AMFMT', 'format of amplitude coef(trace,order)'),
	'ampl_order'            : ('TR-AORD',  'polynomial order of amplitude fits'),
	'bias'                  : ('CCD-BIAS','CCD bias in e-/ADU'),
	'bkg_factor'            : ('TR-FBKG',  'factor used to exclude low-level max.' ),
	'biassec'               : ('BIAS-SEC','raw region containing the bias overscan'),
	'dy_max'                : ('TR-DYMAX', 'max. allowed vert. dev. of local max.'),
	'dy_min'                : ('TR-DYMIN', 'min. allowed vert. dev. of local max.'),
	'exptime'               : ('EXPTIME','exposure time [s]'),
	'gain'                  : ('CCD-GAIN','gain of CCD [e-/ADU]'),
	'index'                 : ('IF-INDEX','index of a single traced spectrum'),
	'index_format'          : ('IF-IDFMT','format for reading fibre indices'),
	'inner_diameter'        : ('IF-DIN',  'inner diameter of a single fibre'),
	'inner_diameter_format' : ('IF-DIFMT','format for reading inner diameters'),
	'label'                 : ('IF-LABEL','label of a single spectrum'),
	'label_format'          : ('IF-LFMT', 'format for reading fibre labels'),
	'mid_slice'             : ('TR-SLICE', 'starting vert. slice to find local max.'),
	'number_slices'         : ('TR-NSLIC', 'number of vert. slices'),
	'number_traces'         : ('TR-NUMB',  '(desired) number of horiz. traces'),
	'outer_diameter'        : ('IF-DOUT', 'outer diameter of a single fibre'),
	'outer_diameter_format' : ('IF-DOFMT','format for reading outer diameters'),
	'pixel1'                : ('IF-PIX1', 'lower integration range [pix]'),
	'pixel2'                : ('IF-PIX2', 'upper integration range [pix]'),
	'ron'                   : ('CCD-RON','readout noise of CCD [e-]'),
	'sigma'                 : ('TR-SIGMA', 'median vertical width of traces'),
	'sigma_factor'          : ('TR-SFACT', 'sigma factor for fat fibres'),
	'sigma_format'          : ('IF-SGFMT','format for reading fibre sigmas'),
	'sigma_kappa'           : ('TR-SKAPA', 'kappa clipping for finding fat fibres'),
	'sigma_fit0'            : ('TR-SIGC0', 'c0, sigma=c0+c1*x+c2*x^2+c3*x^3'),
	'sigma_fit1'            : ('TR-SIGC1', 'c1, sigma=c0+c1*x+c2*x^2+c3*x^3'),
	'sigma_fit2'            : ('TR-SIGC1', 'c2, sigma=c0+c1*x+c2*x^2+c3*x^3'),
	'sigma_fit3'            : ('TR-SIGC1', 'c3, sigma=c0+c1*x+c2*x^2+c3*x^3'),
	'sigma_order'           : ('TR-SORDR', 'order of sigma(x) polynomial'),
	'spacing'               : ('IF-SPACE','median vertical spacing of spectra'),
	'spacing_format'        : ('IF-SPFMT','format for reading fibre spacings'),
	'trace_bias'            : ('TR-BIAS',  'assumed trace background' ),
	'trace_coef'            : (None,'trace coefficient c(index,ord)'),
	'trace_format'          : ('TR-TRFMT', 'format of trace coef(trace,order)'),
	'trace_index'           : ('TR-INDEX', 'index of traced spectrum'),
	'trace_order'           : ('TR-ORDER', 'polynomial order of trace position fit'),
	'trimsec'               : ('TRIM-SEC','raw region containing actual data'),
	'wave1'                 : ('IF-WAVE1','lower integration range [nm]'),
	'wave2'                 : ('IF-WAVE2','upper integration range [nm]'),
	'window_centroid'       : ('TR-WCNTD', 'width of vert. centroid window'),
	'window_max'            : ('TR-WMAX',  'width of vert. window used to find max.'),
	'window_profile'        : ('TR-WPROF', 'width of vert. profile window'),
	'x_max'                 : ('TR-XMAX',  'maximum traced x [pix]'),
	'x_min'                 : ('TR-XMIN',  'minimum traced x [pix]'),
	'xpos'                  : ('IF-XPOS', 'focal plane x-pos. of a single fibre'),
	'xpos_format'           : ('IF-XFMT', 'format for reading focal plane pos.'),
	'ypos'                  : ('IF-YPOS', 'focal plane y-pos. of a single fibre'),
	'ypos_format'           : ('IF-YFMT', 'format for reading focal plane pos.')
	}


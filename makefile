# makefile for pyFU

# This makefile executes a complete IFU reduction for a fake sky image (no flat, no lamp).
# The IFU and reduction parameters are all stored in a YAML file.

PLOT= --plot
PYFUDIR=./MORISOT
IMAGES=./MORISOT
ATLAS=./TEST/liege.fits
YAML=${PYFUDIR}/morisot.yaml
TRACE=${PYFUDIR}/trace.yaml
FINAL=${PYFUDIR}/calibrated.fits

all : solar calib trace extract wavcal image rebin

solar :
	@echo "Creating a solar reference spectrum ..."
	python pyFU/solar.py --yaml ${YAML} ${PLOT}

calib :
	@echo "Calibrating the spectral image ..."
	python pyFU/calib.py --yaml ${YAML} --plot

trace :
	@echo "Tracing the IFU image ..."
	python pyFU/trace.py --yaml ${YAML} ${PLOT}

extract :
	@echo "Extracting the IFU spectra ..."
	python pyFU/extract.py --yaml ${YAML} ${PLOT} --trace ${TRACE}

wavcal :
	@echo "Wavelength calibrating the extracted IFU spectra ..."
	python pyFU/wavcal.py --yaml ${YAML} ${PLOT}

image :
	@echo "Combining the calibrated spectra into an IFU image ..."
	python pyFU/image.py --yaml ${YAML}  --outfile ./${IMAGES}/wavelength_image.png

rebin :
	@echo "Rebinning the calibrated IFU spectra ... "
	python pyFU/rebin.py --yaml ${YAML} ${PLOT}

pixel_image :
	@echo "Combining the extracted spectra into an IFU image ..."
	python pyFU/image.py --yaml ${YAML} --infile ./${PYFUDIR}/extracted.fits --outfile ./${IMAGES}/pixel_image.png

display :
	@echo "Displaying the final calibrated spectra ..."
	python pyFU/display.py --table ${FINAL}

fake :
	@echo "Creating a fake IFU image ..."
	python pyFU/fake.py --yaml ${YAML} ${PLOT} --full

scatter :
	@echo "Fitting the scattered light ..."
	python pyFU/scatter.py --yaml ${YAML} --plot --fit --order 5

classify :
	@echo "Classifying the final calibrated spectra ..."
	python pyFU/classify.py

virus : virus_sub virus_trace virus_extract

virus_sub :
	python pyFU/maths.py VIRUS/rawff.fits = VIRUS/domeflat.fits.gz - VIRUS/bias.fits.gz

virus_trace :
	python pyFU/trace.py --yaml VIRUS/virus.yaml --plot --infile VIRUS/rawff.fits --save VIRUS/trace.yaml

virus_extract :
	python pyFU/extract.py --yaml VIRUS/virus.yaml --plot --infile VIRUS/rawff.fits --outfile VIRUS/extff.fits

test_images :
	python pyFU/image.py --yaml ${YAML} --infile ./${PYFUDIR}/extracted.fits --outfile ./${IMAGES}/pixel_image.png
	python pyFU/image.py --yaml ${YAML} --outfile ./${IMAGES}/wavelength_image.png

archive :
	zip -r pyFU.zip README.* makefile ${PYFUDIR}/*.yaml *.py

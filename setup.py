#!/usr/bin/evn python3
from setuptools import setup, find_packages

setup(
	name='pyFU',
	version='0.1',
	description='simple IFU spectral image reduction software',
	author='Frederic V. Hessman',
	author_email='hessman@astro.physik.uni-goettingen.de',
	requires=['argparse','astropy','bisect','matplotlib','numpy','parse','scipy','skimage','yaml']
	)

